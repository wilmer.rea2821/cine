
    <!-- Instagram Post Section Begin -->
    <section class="instagram-post-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="section-title">
                        <h5>POSTAL DE PELICULAS</h5>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                            <img  src="<?php echo base_url(); ?>/assets/images/pelicula1.jpg"  alt="Los Angeles" style="height:200px; width:100%"; >
                        </div>
                        <div class="ip-text">

                            <h5><a href="#">JUMANJI</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                <li><i class="fa fa-comment-o"></i> 20</li>
                            </ul>
                            <p>Jumanji es una película estadounidense de 1995 dirigida por Joe Johnston y con actuación de Robin Williams, Bonnie Hunt, Kirsten Dunst y Bradley Pierce.</p>
                        </div>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                              <img  src="<?php echo base_url(); ?>/assets/images/pelicula2.jpg"  alt="Los Angeles" style="height:200px; width:100%"; >
                        </div>
                        <div class="ip-text">

                            <h5><a href="#">AVATAR</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                <li><i class="fa fa-comment-o"></i> 20</li>
                            </ul>
                            <p>Jake Sully y Ney'tiri han formado una familia y hacen todo lo posible por permanecer juntos. Sin embargo, deben abandonar su hogar y explorar las regiones de Pandora cuando una antigua amenaza reaparece.</p>
                        </div>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                              <img  src="<?php echo base_url(); ?>/assets/images/pelicula3.jpg"  alt="Los Angeles" style="height:200px; width:100%"; >
                        </div>
                        <div class="ip-text">

                            <h5><a href="#">Terror</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                <li><i class="fa fa-comment-o"></i> 20</li>
                            </ul>
                            <p>El Conjuro está basado en una historia real, documentada por el matrimonio Warren, integrado por dos investigadores de fenómenos paranormales de renombre, quienes acuden a investigar el caso más tenebroso </p>
                        </div>
                    </div>
                    <div class="ip-item">
                        <div class="ip-pic">
                              <img  src="<?php echo base_url(); ?>/assets/images/pelicula4.jpg"  alt="Los Angeles" style="height:200px; width:100%"; >
                        </div>
                        <div class="ip-text">

                            <h5><a href="#">GUERRA Z</a></h5>
                            <ul>
                                <li>by <span>Admin</span></li>
                                <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                <li><i class="fa fa-comment-o"></i> 20</li>
                            </ul>
                            <p>Cuando una pandemia de zombis amenaza con destruir a la humanidad, un exinvestigador de Naciones Unidas es obligado a regresar al servicio para intentar descubrir la fuente de la infección.</p>
                        </div>
                    </div>
                    <div class="pagination-item">
                        <a href="#"><span>1</span></a>
                        <a href="#"><span>2</span></a>
                        <a href="#"><span>3</span></a>
                        <a href="#"><span>Next</span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-7">
                    <div class="sidebar-option">
                        <div class="insta-media">
                            <div class="section-title">
                                <h5>RAPIDOS Y FURIOSOS</h5>
                            </div>
                            <div class="insta-pic">

                                <img src="<?php echo base_url(); ?>/assets/images/pelicula5.jpg" alt="">
                                <img src="<?php echo base_url(); ?>/assets/images/pelicula6.jpg" alt="">
                                <img src="<?php echo base_url(); ?>/assets/images/pelicula7.png" alt="">
                                <img src="<?php echo base_url(); ?>/assets/images/pelicula8.jpg" alt="">
                            </div>
                        </div>

                        <div class="subscribe-option">
                            <div class="section-title">
                                <h5>Subscribirse</h5>
                            </div>
                            <p>Formulario de registro.</p>
                            <form action="#">
                                <input type="text" placeholder="Name">
                                <input type="text" placeholder="Email">
                                <button type="submit"><span>Subscribe</span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
