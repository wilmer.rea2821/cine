<br>
<br>
<center>
<b><div class="col-md-12 text-center well">
  <h3>BIENVENIDOS A CONTINUACIÓN PUEDES VISUALIZAR LOS ASIENTOS DISPONIBLES</h3></div></b>
</center>
<hr>

<div class="container overflow-hidden">
  <div class="row gx-5">
    <div class="col">
     <div class="p-3 border bg-light">

       <?php if ($asientos): ?>
         <table id="tbl-asientos" class="table table-bordered table-striped table-hover" style="color:#000000">
           <thead>
             <tr>
               <th class="text-center"><i class="fas fa-id-card"></i>ID: </th>
               <th class="text-center"><i class="fas fa-couch"></i>FILA : </th>
               <th class="text-center"><i class="fas fa-couch"></i>NUMERO DE ASIENTO: </th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($asientos->result() as $asientoTemporal): ?>
               <tr>
                 <td class="text-center" ><?php echo $asientoTemporal->id_asi_eda?></td>
                 <td class="text-center" ><?php echo $asientoTemporal->fila_asi_eda?></td>
                 <td class="text-center" ><?php echo $asientoTemporal->numero_asi_eda?></td>
               </tr>
             <?php endforeach; ?>
           </tbody>
         </table>
       <?php else: ?>
         <div class="aler alert-danger">
           No se encontraron asientos registrados.
         </div>
       <?php endif; ?>
       <script type="text/javascript">
       $("#tbl-asientos").DataTable();
       </script>

     </div>
    </div>
  </div>
</div>
