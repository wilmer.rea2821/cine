<br>
<center>
<b><div class="col-md-12 text-center well">
  <h3></h3></div></b>
</center>
<hr>

<br><br>
<!-- Button trigger modal -->
<center><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalNuevaBoletos">
  Comprar boletos
</button></center>

<!-- Modal -->
<div class="modal fade" id="modalNuevaBoletos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">COMPRA DE BOLETOS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="frm_nueva_boletos"class="" enctype="multipart/form-data" action="<?php echo site_url('boletos/guardarFunciones') ?>" method="post">
          <b>Fecha</b> <br>
          <div class="col-md-10">
            <input class="form-control" type="date" name="fecha_emision_ven_eda" id="fecha_emision_ven_eda" value="">
          </div>
          <br>
          <b>Total Boletos</b> <br>
          <div class="col-md-10">
            <input class="form-control" type="number" name="total_ven_eda" id="total_ven_eda" value="">

          </div>
          <br>
          <b>Asientos</b> <br>
          <div class="col-md-10">
  <select class="form-control" name="fk_id_asi_eda" id="fk_id_asi_eda" required data-live-search="true">
    <!-- cargar opciones de <select class="" name="">  </select> -->
    <option value="">-------------selecciones el Asientos-----------</option>
    <?php if ($listadoasientos): ?>
      <?php foreach ($listadoasientos->result() as $asientoTemporal): ?>
        <option value="<?php echo $asientoTemporal->id_asi_eda;?>">
        <?php echo $asientoTemporal->fila_asi_eda; ?>
        --
        <?php echo $asientoTemporal->numero_asi_eda; ?>



      <?php endforeach; ?>

    <?php endif; ?>

</select>
</div>
          <br>
          <b>Pelicula</b> <br>
          <div class="col-md-10">
  <select class="form-control" name="fk_id_pel_eda" id="fk_id_pel_eda" required data-live-search="true">
    <!-- cargar opciones de <select class="" name="">  </select> -->
    <option value="">-------------selecciones la pelicula-----------</option>
    <?php if ($listadoPeliculas): ?>
      <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
        <option value="<?php echo $peliculaTemporal->id_pel_eda;?>">
        <?php echo $peliculaTemporal->nombre_pel_eda; ?>
        -
        <?php echo $peliculaTemporal->tiempo_pel_eda; ?>
        </option>
      <?php endforeach; ?>

    <?php endif; ?>

</select>
</div>
          <br>
          <b>Tarifa</b> <br>
          <div class="col-md-10">

  <select class="form-control"   name="fk_id_tar_eda" id="fk_id_tar_eda" required data-live-search="true">
    <!-- cargar opciones de <select class="" name="">  </select> -->
    <option value="">-------------selecciones la Tarifa-----------</option>
    <?php if ($listadoTarifa): ?>
      <?php foreach ($listadoTarifa->result() as $tarifaTemporal): ?>
        <option value="<?php echo $tarifaTemporal->id_tar_eda;?>">
        <?php echo $tarifaTemporal->adulto_tar_eda; ?>
        --
        <?php echo $tarifaTemporal->estudiante_tar_eda; ?>

        </option>

      <?php endforeach; ?>

    <?php endif; ?>

</select>
</div>


          <br>
          <button type="submit" name="button" class="btn btn-success"> <i class="glyphicon glyphicon-ok"></i> Comprar</button>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

      </div>
    </div>
  </div>
</div>
<br><br>
<section class="latest-preview-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h5>peliculas DISPONIBLES PARA DISFRUTAR DE TU PELICULA</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="lp-slider owl-carousel">
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div ><img  src="<?php echo base_url(); ?>/assets/images/estreno1.jpg"  alt="Los Angeles" style="height:100%; width:100%"; >
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Ant-Man and the Wasp: Quantumania es una próxima película de superhéroes estadounidense basada en los personajes de Marvel Comics, Scott Lang / Ant-Man y Hope van Dyne </a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div ><img  src="<?php echo base_url(); ?>/assets/images/estreno2.jpg"  alt="Los Angeles" style="height:100%; width:100%"; >
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Aquaman forja una alianza incómoda con un aliado poco probable para salvar a Atlantis y al resto del planeta de la muerte inminente.</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/estreno3.jpg"  alt="Los Angeles" style="height:100%; width:100%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Traducción del inglés-Iron Man (vol. 4) es una serie de cómics publicada desde enero de 2005 hasta enero de 2009 por Marvel Comics. Presentó al superhéroe Iron Man en los primeros 32 números y War Machine en los tres últimos.</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/estreno4.jpg"  alt="Los Angeles" style="height:100%; width:100%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">En ella volveremos a encontrarnos a aquel niño adoptivo que solo tiene que decir la palabra mágica, ¡Shazam! para convertirse en su mejor versión del futuro: un héroe alto y musculoso protagonizado por Zachary Levi. Billy Batson ha vuelto.</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/estreno5.jpg"  alt="Los Angeles" style="height:100%; width:100%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Una joven pareja se ve envuelta en el golpe de Estado en Chile en 1973. Él es secuestrado por la policía secreta de Pinochet, y ella le seguirá la pista hasta una zona al sur del país llamada Colonia Dignidad; el lugar parece una misión de caridad regida por un sacerdote</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/estreno6.jpg"  alt="Los Angeles" style="height:100%; width:100%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">El regreso de la RDA a Pandora, con el fin de retomar su plan extractivista en el nuevo hogar de Jake Sully, quien dejó su cuerpo humano para vivir como Na'vi junto a Neytiri.</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/estreno7.jpg"  alt="Los Angeles" style="height:100%; width:100%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">el regreso de la RDA a Pandora, con el fin de retomar su plan extractivista en el nuevo hogar de Jake Sully, quien dejó su cuerpo humano para vivir como Na'vi junto a Neytiri.</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br><br>
<!-- validaciones-->
<script type="text/javascript">
   $("#frm_nueva_boletos").validate({
     rules:{
       fecha_emision_ven_eda:{
         required:true
       },
       total_ven_eda:{
         required: true,
         minlength:1,
         maxlength:10,
         digits:true
       },
       fk_id_pel_eda:{
         required:true
       },
       fk_id_sal:{
         required:true
       },
       fk_id_hor:{
         required:true

       }


     },
     messages:{
       fecha_emision_ven_eda:{
         required:"porfavor ingrese uana fecha "
       },
       total_ven_eda:{
         required:"porfavor ingrese un valor entero",
         minlength:"Número invalido por favor corrija",
         maxlength:"Número invalido por favor corrija",
         digits:"porfavor ingrese los numeros en el rrango de uno a 10"
       },
       fk_id_pel_eda:{
         required:"selecione una pelicula"
       },
       fk_id_sal:{
         required:"seleccione una sala "
       },
       fk_id_hor:{
         required:"seleccione una hora"
       }

     },


      submitHandler:function(formulario){
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url('boletosP/guardarBoletos') ?>',
          data:$(formulario).serialize(),
          success:function(data){
            var objetoRespuesta=JSON.parse(data);
            if (objetoRespuesta.estado=="ok") {
              Swal.fire('confirmacion',objetoRespuesta.mensaje,'success');
                $("#modalNuevaBoletos").modal("hide");
                // consultarBoletos();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>

<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('configuraciones/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('boletos/editar'); ?>/"+id);
      $("#modalEditarBoletos").modal("show");
      // consultarBoletos();
    }
</script>
