<br>
<br>
<center>
<b><div class="col-md-12 text-center well">
  <h3>BIENVENIDOS A NUESTRO BAR</h3></div></b>
</center>
<hr>
<!-- Latest Preview Section Begin -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <section class="latest-preview-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h5>PRODUCTOS DISPONIBLES PARA DISFRUTAR DE TU PELICULA</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="lp-slider owl-carousel">
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div ><img  src="<?php echo base_url(); ?>/assets/images/cine.jpg"  alt="Los Angeles" style="height:100px; width:50%"; >
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Prodcutos de Cine</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div ><img  src="<?php echo base_url(); ?>/assets/images/cine1.jpg"  alt="Los Angeles" style="height:100px; width:50%"; >
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">crocante Canguil saladito de 85oz. *45 POR CIENTO DE DESCUENTO* EXCLUSIVO PARA COMPRAS POR APP Y WEB</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/cine2.jpg"  alt="Los Angeles" style="height:100px; width:50%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Refrescante gaseosa de 32oz acompañada de un crocante Canguil saladito de 130oz *Rellena tu Canguil GRATIS una vez más el día de tu compra*</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/cine3.jpg"  alt="Los Angeles" style="height:100px; width:50%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">HOT DOG E.F Delicioso Hot Dog de Res o Pollo</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/cine4.jpg"  alt="Los Angeles" style="height:100px; width:50%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Dasani sin gas o con gas</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/cine5.jpg"  alt="Los Angeles" style="height:100px; width:50%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">EXTRA QUESO E.F. 4 oz de exquisito queso Cheddar derretido</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="lp-item">
                            <div > <img  src="<?php echo base_url(); ?>/assets/images/cine6.jpg"  alt="Los Angeles" style="height:100px; width:50%";>
                                <div class="review-loader">
                                    <div class="loader-circle-wrap">
                                        <div class="loader-circle">
                                            <span class="circle-progress" data-cpid="id" data-cpvalue="75"
                                                data-cpcolor="#c20000"></span>
                                            <div class="review-point">7.5</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="lp-text">
                                <h6><a href="#">Deliciosas Hojuelas de maíz acompañadas de salsa de queso Cheddar y una refrescante gaseosa de 22oz.</a></h6>
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> Aug 01, 2019</li>
                                    <li><i class="fa fa-comment-o"></i> 12</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr>
    <br> <br>

<?php if ($listadobar): ?>
  <table id="tbl-bar" class="table table-bordered table-striped table-hover" style="color:#0000000">
    <thead>
      <tr>
        <!-- <th class="text-center">ID: </th> -->
        <th class="text-center"><i class="fas fa-shopping-cart"></i> NOMBRE DE PRODUCTO : </th>
        <th class="text-center"><i class="fas fa-shopping-cart"></i> PRECIO DE PRODUCTO: </th>
        <th class="text-center"><i class="fas fa-shopping-cart"></i> CANTIDAD DE PRODUCTO: </th>
        <th class="text-center"><i class="fa fa-check-square-o" aria-hidden="true"></i> SELECCIONAR PEDIDO: </th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadobar->result() as $barTemporal): ?>
        <tr>
          <!-- <td class="text-center"><?php echo $barTemporal->id_bar_eda?></td> -->
          <td class="text-center"><?php echo $barTemporal->nombre_bar_eda?></td>
          <td class="text-center"><?php echo $barTemporal->precio_bar_eda?></td>
          <td class="text-center"><?php echo $barTemporal->cantidad_bar_eda?></td>
          <td class="text-center">

                <button type="button" onclick="mostrar();"
                    class="btn btn-info">
                      <i class="fas fa-shopping-cart"></i> <i class="fa fa-check-square-o" aria-hidden="true"></i>
                    </button>
              </a></td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <script type="text/javascript">
    function mostrar(){
      swal('Alerta','Este modulo aun sigue en desarrollo','info');
    }
  </script>
<?php else: ?>
  <div class="aler alert-danger">
    No se encontraron asientos registrados.
  </div>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-bar").DataTable();
</script>

 <hr>
