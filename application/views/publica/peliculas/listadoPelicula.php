<br>
<br>
<center>
<b><div class="col-md-12 text-center well">
  <h3>BIENVENIDOS AL STAND DE NUESTRAS PELICULAS</h3></div></b>
</center>
<hr>
<br>
<div class="container overflow-hidden">
  <div class="row gx-5">
    <div class="col">
     <div class="p-3 border bg-light">
       <?php if ($lisPeliculas): ?>
         <table id="tbl-peliculas" class="table table-bordered table-striped table-hover" style="color:#000000">
           <thead>
             <tr>
               <td class="text-center">ID</td>
               <td class="text-center"><i class="fas fa-video"></i>NOMBRE</td>
               <td class="text-center"><i class="fas fa-surprise"></i>TIPO</td>
               <td class="text-center"><i class='fas fa-business-time'></i>TIEMPO</td>
               <td class="text-center"><i class="fas fa-book-reader"></i>DETALLE</td>
               <td class="text-center"><i class="fas fa-times"></i>RESTRICCION</td>
               <td class="text-center"><i class="fa fa-camera"></i>FOTO</td>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($lisPeliculas->result() as $peliculaTemporal): ?>
               <tr>
                 <td class="text-center"><?php echo $peliculaTemporal->id_pel_eda ?></td>
                 <td class="text-center"><?php echo $peliculaTemporal->nombre_pel_eda ?></td>
                 <td class="text-center"><?php echo $peliculaTemporal->tipo_pel_eda ?></td>
                 <td class="text-center"><?php echo $peliculaTemporal->tiempo_pel_eda ?></td>
                 <td class="text-center"><?php echo $peliculaTemporal->detalle_pel_eda ?></td>
                 <td class="text-center"><?php echo $peliculaTemporal->restriccion_pel_eda ?></td>
                 <td class="text-center"><?php if ($peliculaTemporal->foto_pel_eda!=""): ?>
                           <a href="<?php echo base_url('uploads/peliculas').'/'.$peliculaTemporal->foto_pel_eda; ?>"
                             target="_blank">
                             <img src="<?php echo base_url('uploads/peliculas').'/'.$peliculaTemporal->foto_pel_eda; ?>"
                             width="150px" height="150px"
                             alt="">
                           </a>
                         <?php else: ?>
                           N/A
                       <?php endif; ?></td>
               </tr>
             <?php endforeach; ?>
           </tbody>
         </table>
       <?php else: ?>
         <div class="aler alert-danger">
           No se encontraron asientos registrados.
         </div>
       <?php endif; ?>
       <script type="text/javascript">
       $("#tbl-peliculas").DataTable();
       </script>

     </div>
    </div>
</div>
<br>
<br>
