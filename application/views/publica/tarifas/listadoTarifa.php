<br>
<br>
<center>
<b><div class="col-md-12 text-center well">
  <h3>BIENVENIDOS A NUESTRAS TARIFAS</h3></div></b>
</center>
<hr>
<div class="container overflow-hidden">
  <div class="row gy-5">
    <div class="col-6">
      <div class="p-3 border bg-light">
        <?php if ($tarifas): ?>
        <table class="table table-bordered table-striped table-hover" style="color:#000000">
          <thead>
            <tr>
              <th class="text-center"><i class="	fas fa-male">TARIFA ADULTO: </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($tarifas->result() as $tarifasTemporal): ?>
              <tr>
                <td class="text-center">$ <?php echo $tarifasTemporal->adulto_tar_eda ?></td>

              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else: ?>
        <div class="aler alert-danger">
          No se encontraron tarifas registrados.
        </div>
      <?php endif; ?>
    </div>
    <div class="p-3 border bg-light">
      <?php if ($tarifas): ?>
        <table class="table table-bordered table-striped table-hover" style="color:#000000">
          <thead>
            <tr>
              <th class="text-center"><i class="	fas fa-male">TARIFA NIÑOS: </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($tarifas->result() as $tarifasTemporal): ?>
              <tr>
                <td class="text-center">$ <?php echo $tarifasTemporal->ninos_tar_eda ?></td>

              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else: ?>
        <div class="aler alert-danger">
          No se encontraron tarifas registrados.
        </div>
      <?php endif; ?>
    </div>
    <div class="p-3 border bg-light">
      <?php if ($tarifas): ?>
      <table class="table table-bordered table-striped table-hover" style="color:#000000">
        <thead>
          <tr>
            <th class="text-center"><i class="	fas fa-male">TARIFA PERSONAS DE LA TERCERA EDAD: </th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($tarifas->result() as $tarifasTemporal): ?>
            <tr>
              <td class="text-center">$ <?php echo $tarifasTemporal->tercera_tar_eda ?></td>

            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <div class="aler alert-danger">
        No se encontraron tarifas registrados.
      </div>
    <?php endif; ?>
    </div>
    </div>
    <div class="col-6">
      <div class="p-3 border bg-light">
        <?php if ($tarifas): ?>
        <table class="table table-bordered table-striped table-hover" style="color:#000000">
          <thead>
            <tr>
              <th class="text-center"><i class="	fas fa-male">TARIFA ESTUDIANTE: </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($tarifas->result() as $tarifasTemporal): ?>
              <tr>
                <td class="text-center">$ <?php echo $tarifasTemporal->estudiante_tar_eda ?></td>

              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else: ?>
        <div class="aler alert-danger">
          No se encontraron tarifas registrados.
        </div>
      <?php endif; ?>
      </div>
      <div class="p-3 border bg-light">
        <?php if ($tarifas): ?>
        <table class="table table-bordered table-striped table-hover" style="color:#000000">
          <thead>
            <tr>
              <th class="text-center"><i class="fab fa-accessible-icon">TARIFA PERSONAS CON DISCAPACIDAD: </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($tarifas->result() as $tarifasTemporal): ?>
              <tr>
                <td class="text-center">$ <?php echo $tarifasTemporal->discapacitados_tar_eda ?></td>

              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else: ?>
        <div class="aler alert-danger">
          No se encontraron tarifas registrados.
        </div>
      <?php endif; ?>
      </div>
    </div>
