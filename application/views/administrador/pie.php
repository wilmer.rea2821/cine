<!-- Footer Start -->
<div class="container-fluid pt-4 px-4">
    <div class="bg-secondary rounded-top p-4">
        <div class="row">
            <div class="col-12 col-sm-6 text-center text-sm-start">
                &copy; <a href="#">Alex Travez</a>, David Rea, Edith Cumbajin.
            </div>
            <div class="col-12 col-sm-6 text-center text-sm-end">
                <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                DESARROLLADO POR <a href="https://htmlcodex.com">ESTUDIANTES UTC</a>
                <br>DISTRIBUIDO POR: <a href="https://themewagon.com" target="_blank">UNIVERSIDAD TÉCNICA DE COTOPAXI</a>
            </div>
        </div>
    </div>
</div>
<!-- Footer End -->
</div>
<!-- Content End -->


<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
</div>

<!-- JavaScript Libraries -->
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('assets/administracion/'); ?>lib/chart/chart.min.js"></script>
<script src="<?php echo base_url('assets/administracion/'); ?>lib/easing/easing.min.js"></script>
<script src="<?php echo base_url('assets/administracion/'); ?>lib/waypoints/waypoints.min.js"></script>
<script src="<?php echo base_url('assets/administracion/'); ?>lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/administracion/'); ?>lib/tempusdominus/js/moment.min.js"></script>
<script src="<?php echo base_url('assets/administracion/'); ?>lib/tempusdominus/js/moment-timezone.min.js"></script>
<script src="<?php echo base_url('assets/administracion/'); ?>lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js"></script>

<!-- Template Javascript -->
<script src="<?php echo base_url('assets/administracion/'); ?>js/main.js"></script>
<?php if ($this->session->flashdata('confirmacion')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'CONFIRMACIÓN', //titulo
            '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
            'success' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>


    <?php if ($this->session->flashdata('error')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'ERROR', //titulo
            '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
            'error' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>
<style>
    label.error{
        border:1px solid white;
        color:white;
        background-color:#E15B69;
        padding:5px;
        padding-left:15px;
        padding-right:15px;
        font-size:12px;
        opacity: 0;
          /visibility: hidden;/
          /position: absolute;/
          left: 0px;
          transform: translate(0, 10px);
          /background-color: white;/
          /padding: 1.5rem;/
          box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
          width: auto;
          margin-top:30px !important;


          z-index: 10;
          opacity: 1;
          visibility: visible;
          transform: translate(0, -20px);
          transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
          border-radius:10px;
          width:100%;

    }

    input.error{
        border:1px solid #E15B69;
    }


    select.error{
        border:1px solid #E15B69;
    }

    label.error:before{
         position: absolute;
          z-index: -1;
          content: "";
          right: calc(90% - 10px);
          top: -8px;
          border-style: solid;
          border-width: 0 10px 10px 10px;
          border-color: transparent transparent #E15B69 transparent;
          transition-duration: 0.3s;
          transition-property: transform;
    }


</style>
</style>
<!-- 
<script src="https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-table@1.21.2/dist/bootstrap-table.min.js"></script> -->
</body>

</html>
