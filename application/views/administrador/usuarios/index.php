<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalNuevoUsuario">
   <i class="fa fa-plus-square"></i>  REGISTRAR USUARIOS
</button></center>
<br><hr>
<legend class="text-center" >
<i class="fa fa-user"></i>
<b style="color:#ffffff">GESTIÓN DE USUARIOS</b>
</legend>
<hr>

<!-- The Modal -->
<div class="modal" id="modalNuevoUsuario">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="color:#000000">REGISTRO DE USUSARIOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form  id="frm_nuevo_usuario" class="" action="<?php echo site_url("usuarios/guardar"); ?>" method="post">
          <i class="fa fa-user" aria-hidden="true"></i><b> APELLIDO</b>
          <br>
          <input type="text" id="apellido_usu_eda" name="apellido_usu_eda" value="" placeholder="Ingrese los apellidos del usuario" class="form-control"> <br>
        <i class="fas fa-user"></i><b> NOMBRE</b>
          <br>
          <input type="text" id="nombre_usu_eda" name="nombre_usu_eda" value="" placeholder="Ingrese el nombre del usuario" class="form-control"> <br>
          <i class="fa fa-envelope" aria-hidden="true"></i> <b>EMAIL</b>
          <br>
          <input type="text" id="email_usu_eda" name="email_usu_eda" value="" placeholder="Ingrese el email del usuario" class="form-control"> <br>
          <i class="fas fa-key"></i> <b> PASSWORD</b>
          <br>
          <input type="password" id="password_usu_eda" name="password_usu_eda" value="" placeholder="Ingrese una contraseña" class="form-control"> <br>
          <i class="fa fa-user-secret" aria-hidden="true"></i> <b>PERFIL</b>
          <br>
          <select class="form-control" name="perfil_usu_eda" id=perfil_usu_eda"">
              <option value= " ">--Seleccione una opcion</option>
              <option value="ADMINISTRADOR">Administrador</option>
              <option value="EMPLEADO">Empleado</option>
          </select> <br>
          <button type="submit"class="btn btn-success" name="button" onclick=""> <i class="fa fa-check-square"></i> GUARDAR</button>
        </form>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<div class="container">
  <div id="contenedor-listado-usuarios">
    <!-- fa-lg(tamaño)/fa-6x(tamaño)/ fa-spin(efecto de rotar)-->
  </div>
</div>



<div class="modal" id="modalEditarUsuario">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

        <h4 class="modal-title" style="color:#000000">ACTUALIZACIÓN DE USUAIOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<!-- inicio script funcion consultarProfesores id(#) clase(.) -->
<script type="text/javascript">
  function consultarUsuarios(){  // se carga la vista cuando se llama a la funcion
    $("#contenedor-listado-usuarios")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('usuarios/listado');?>");

  }
  consultarUsuarios();
</script>
<script >

    function eliminar(id_usu_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("usuarios/borrar"); ?>',
              data:{"id_usu_eda":id_usu_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                   $("#modalNuevoUsuario").modal("hide");
                    consultarUsuarios();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }


              }
            });

    }

</script>
<script type="text/javascript">
   $("#frm_nuevo_usuario").validate({
      rules:{
          apellido_usu_eda:{
            required:true
          },
          nombre_usu_eda:{
            required: true
          },
          email_usu_eda:{
            required: true
          },
          password_usu_eda:{
            required: true

        },
        perfil_usu_eda:{
          required: true
        }
      },
      messages:{
        apellido_usu_eda:{
          required:"Por favor ingrese el apellido del usuario"
        },
        nombre_usu_eda:{
          required: "Por favor ingrrese el nombre del usuario"
        },
        email_usu_eda:{
          required: "Por favor ingrese el email de registro"
        },
        password_usu_eda:{
          required:"Por favor ingrese una contraseña"
        },

      perfil_usu_eda:{
        required:"Por favor selecione el perfil"
      }

    },
      submitHandler:function(formulario){

        var formData = new FormData($(formulario)[0]);
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url("usuarios/guardar"); ?>',
          data:formData,
          contentType: false,
          processData: false,
          success:function(data){
            var objeto=JSON.parse(data);
            if (objeto.estado=="ok") {
              Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                $("#modalNuevoUsuario").modal("hide");
                consultarUsuarios();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>

<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('usuarios/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('usuarios/editar'); ?>/"+id);
      $("#modalEditarUsuario").modal("show");
      consultarUsuarios();
    }
</script>
