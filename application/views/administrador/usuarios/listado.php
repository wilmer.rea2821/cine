
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<hr>
<?php if ($listadoUsuarios): ?>
  <table id="tbl-usuarios" class="table table-striped table-bordered table-hover" style="color:#ffffff">
    <thead>
      <tr>
        <td class="text-center"><i class="fas fa-id-card"></i> ID</td>
        <td class="text-center"><i class="fas fa-user"></i> APELLIDO</td>
        <td class="text-center"> <i class="fas fa-user"></i> NOMBRE</td>
        <td class="text-center"><i class="fa fa-envelope" aria-hidden="true"></i>  EMAIL</td>
        <td class="text-center"><i class="fas fa-key"></i>  PASSWORD</td>
        <td class="text-center"><i class="fa fa-user-secret" aria-hidden="true"></i> PERFIL</td>
        <td class="text-center">ACCIONES</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoUsuarios->result() as $usuarioTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $usuarioTemporal->id_usu_eda ?></td>
          <td class="text-center"><?php echo $usuarioTemporal->apellido_usu_eda?></td>
          <td class="text-center"><?php echo $usuarioTemporal->nombre_usu_eda ?></td>
          <td class="text-center"><?php echo $usuarioTemporal->email_usu_eda ?></td>
          <td class="text-center"><?php echo $usuarioTemporal->password_usu_eda ?></td>
          <td class="text-center"><?php echo $usuarioTemporal->perfil_usu_eda ?></td>
          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $usuarioTemporal->id_usu_eda ?>);">
                  <i class="fa fa-pen"></i>
                </a>
  <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>
                <a href="<?php echo site_url('usuarios/borrar'); ?>//<?php echo $usuarioTemporal->id_usu_eda ?>" class="btn btn-danger" onclick="mostrar();" disabled="disabled">
                    <i class="fa fa-trash"></i>

                  </a>
                  <?php endif; ?>
          </td>
        </tr>
        <script type="text/javascript">
          function mostrar(){
            swal('Alerta','Esta seguro de eliminar a <?php echo $usuarioTemporal->nombre_usu_eda ?>','info');
          }
        </script>

        <script type="text/javascript">
        $("#tbl-usuarios").dataTable();

        </script>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
<h3>
  <b>No existe Usuarios</b>
</h3>
<?php endif; ?>

<script type="text/javascript">

$("#tbl-usuarios").dataTable();
</script>
