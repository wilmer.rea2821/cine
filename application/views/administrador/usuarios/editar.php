<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_usuarios" enctype="multipart/form-data" class="" action="<?php echo site_url("usuarios/actualizarUsuarios"); ?>" method="post">
  <input type="hidden" name="id_usu_eda" value="<?php echo $usuariosEditar->id_usu_eda; ?>">
<i class="fa fa-user" aria-hidden="true"></i>  <b> APELLIDO</b>
  <br>
  <input type="text" id="apellido_usu_eda" name="apellido_usu_eda" value="<?php echo $usuariosEditar->apellido_usu_eda ?>" placeholder="Ingrese los apellidos del usuario" class="form-control"> <br>
   <i class="fa fa-user" aria-hidden="true"></i><b> NOMBRE</b>
  <br>
  <input type="text" id="nombre_usu_eda" name="nombre_usu_eda" value="<?php echo $usuariosEditar->nombre_usu_eda ?>" placeholder="Ingrese el nombre del usuario" class="form-control"> <br>
   <i class="fa fa-envelope" aria-hidden="true"></i> <b>EMAIL</b>
  <br>
  <input type="text" id="email_usu_eda" name="email_usu_eda" value="<?php echo $usuariosEditar->email_usu_eda ?>" placeholder="Ingrese el email del usuario" class="form-control"> <br>
<i class="fas fa-key"></i>  <b> PASSWORD</b>
  <br>
  <input type="password" id="password_usu_eda" name="password_usu_eda" value="<?php echo $usuariosEditar->password_usu_eda ?>" placeholder="Ingrese una contraseña" class="form-control"> <br>
<i class="fa fa-user-secret" aria-hidden="true"></i>  <b>PERFIL</b>
  <br>
  <input type=""  id="perfil_usu_eda" name="perfil_usu_eda" value="<?php echo $usuariosEditar->perfil_usu_eda ?>" placeholder="Administrador" class="form-control"> <br>

  <button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> Actualizar
      </button>
    </form>
<script type="text/javascript">
function actualizar(){
  $.ajax({
    url:$("#frm_editar_usuarios").prop("action"),
    data:$("#frm_editar_usuarios").serialize(),
    type:"post",
    success:function(data){
      consultarUsuarios();
      $("#modalEditarUsuario").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarUsuario").modal("hide");
          consultarUsuarios();

      }else{
        Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }
    }
  });
}
</script>
