<?php if ($listadoLogs): ?>
  <table id="tabla-funciones" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-center">ID</td>
        <td class="text-center">detalle</td>
        <td class="text-center">fecha</td>
        <td class="text-center">usuario</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoLogs->result() as $boletosTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $boletosTemporal->id_ven_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->fecha_emision_ven_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->total_ven_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->fila_asi_eda?></td>
          <td class="text-center"><?php echo $boletosTemporal->nombre_bar_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->id_tar_eda ?></td>
          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $boletosTemporal->id_ven_eda ?>);">
                  <i class="glyphicon glyphicon-edit"></i>
                  Editar
                </a>

                <a href="<?php echo site_url('boletos/borrar'); ?>/<?php echo $boletosTemporal->id_ven_eda ?>" class="btn btn-danger" onclick="return confirm">
                  <i class="glyphicon glyphicon-trash"></i>
                  Eliminar
                </a>
          </td>
        </tr>
        <script type="text/javascript">
        $("#tabla-boletos").dataTable();

        </script>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
<h3>
  <b>No existe Funciones</b>
</h3>
<?php endif; ?>
