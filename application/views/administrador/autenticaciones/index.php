<?php if ($listadoslogs): ?>
  <table id="tabla-funciones" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-center">ID</td>
        <td class="text-center">detalle</td>
        <td class="text-center">fecha</td>
        <td class="text-center">usuario</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoslogs->result() as $boletosTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $boletosTemporal->id_log_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->descripcion_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->fecha_hora_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->nombre_usu_eda,"-",$boletosTemporal->apellido_usu_eda ?></td>

          <td class="text-center">


          </td>
        </tr>
        <script type="text/javascript">
        $("#tabla-boletos").dataTable();

        </script>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
<h3>
  <b>No existe Funciones</b>
</h3>
<?php endif; ?>
