<?php if ($listadoBoletos): ?>
  <table id="tabla-boletos" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-center">ID</td>
        <td class="text-center">FECHA EMISION</td>
        <td class="text-center">TOTAL</td>
        <td class="text-center">ASIENTOS</td>
        <td class="text-center">BAR</td>
        <td class="text-center">TARIFA</td>
        <td class="text-center">ACCIONES</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoBoletos->result() as $boletosTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $boletosTemporal->id_ven_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->fecha_emision_ven_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->total_ven_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->fila_asi_eda,"--",$boletosTemporal->numero_asi_eda?></td>
          <td class="text-center"><?php echo $boletosTemporal->nombre_pel_eda ?></td>
          <td class="text-center"><?php echo $boletosTemporal->id_tar_eda ?></td>
          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $boletosTemporal->id_ven_eda ?>);">
                  <i class="fa fa-pen"></i>
                </a>
                <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>

                <a onclick="eliminar(<?php echo $boletosTemporal->id_ven_eda ?>);" class="btn btn-danger">
                  <i class="fa fa-trash"></i>
                  
                </a>
                <?php endif; ?>
          </td>
        </tr>
        <script type="text/javascript">
        $("#tabla-boletos").dataTable();

        </script>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
<h3>
  <b>No existe Funciones</b>
</h3>
<?php endif; ?>
<script type="text/javascript">
$("#tabla-boletos").dataTable();

</script>
