
<div class="container">
  <?php if ($ventaBoletos): ?>
    <form id="frm_editar_boletos" class="" action=" <?php echo site_url('boletos/procesoActualizar') ?>" method="post">
      <input type="hidden" name="id_ven_eda" value="<?php echo $ventaBoletos->id_ven_eda ?>">
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Fecha: </label>
        </div>
        <div class="col-md-7">
          <input class="form-control" type="date" name="fecha_emision_ven_eda" id="fecha" value="">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Total boletos: </label>
    </select>
        </div>
        <div class="col-md-7">
          <input class="form-control" type="number" name="total_ven_eda" id="total" value="">
        </div>

      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Asientos: </label>
    </select>
        </div>
        <div class="col-md-7">
          <select class="form-control selectpicker" name="fk_id_asi_eda" id="fk_id_tipo" required data-live-search="true">
            <option value="">-------------selecciones el Asientos-----------</option>
            <?php if ($listadoasientos): ?>
              <?php foreach ($listadoasientos->result() as $asientoTemporal): ?>
                <option value="<?php echo $asientoTemporal->id_asi_eda;?>">
                <?php echo $asientoTemporal->fila_asi_eda; ?>
                --
                <?php echo $asientoTemporal->numero_asi_eda; ?>



              <?php endforeach; ?>

            <?php endif; ?>
          </select>
        </div>

      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Peliculas: </label>
        </div>
        <div class="col-md-7">
          <select class="form-control" name="fk_id_pel_eda" id="fk_pel" required data-live-search="true">
            <option value="">-------------selecciones la pelicula-----------</option>
            <?php if ($listadoPeliculas): ?>
              <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
                <option value="<?php echo $peliculaTemporal->id_pel_eda;?>">
                <?php echo $peliculaTemporal->nombre_pel_eda; ?>
                -
                <?php echo $peliculaTemporal->tiempo_pel_eda; ?>
                </option>
              <?php endforeach; ?>

            <?php endif; ?>

          </select>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Tarifas: </label>
        </div>
        <div class="col-md-7">
          <select class="form-control" name="fk_id_tar_eda" id="fk_id_dea_eda" required data-live-search="true">
            <!-- cargar opciones de <select class="" name="">  </select> -->
            <option value="">-------------selecciones el Tarifa-----------</option>
            <?php if ($listadoTarifa): ?>
              <?php foreach ($listadoTarifa->result() as $tarifaTemporal): ?>
                <option value="<?php echo $tarifaTemporal->id_tar_eda;?>">
                <?php echo $tarifaTemporal->adulto_tar_eda; ?>
                --
                <?php echo $tarifaTemporal->estudiante_tar_eda; ?>

                </option>

              <?php endforeach; ?>

            <?php endif; ?>

          </select>
        </div>
      </div>
      <br>
       <div class="row text-center">
      <div class="col-md-7">
        <button type="submit" name="button" onclick="actualizar();" class="btn btn-success">
          <i class="glyphicon glyphicon-refresh"></i>
          ACTUALIZAR
        </button>

      </div>
    </div>
    </form>

  </div>
  <?php else: ?>
    <div class="alert alert-danger">
      <b>No se encontro la matricula</b>

    </div>
  <?php endif; ?>

  <script type="text/javascript">
  $("#frm_editar_boletos").validate({
    rules:{
      fecha:{
        required:true
      },
      total:{
        required: true,
        minlength:1,
        maxlength:10,
        digits:true
      },
      fk_id_tipo:{
        required:true
      },
      fk_bar:{
        required:true
      },
      fk_id_dea_eda:{
        required:true

      }


    },
    messages:{
      fecha:{
        required:"porfavor ingrese uana fecha "
      },
      total:{
        required:"porfavor ingrese un valor entero",
        minlength:"Número invalido por favor corrija",
        maxlength:"Número invalido por favor corrija",
        digits:"porfavor ingrese los numeros en el rrango de uno a 10"
      },
      fk_id_tipo:{
        required:"selecione una pelicula"
      },
      fk_bar:{
        required:"seleccione una sala "
      },
      fk_id_dea_eda:{
        required:"seleccione una hora"
      }

    },

  </script>
<!-- validacion -->
<script type="text/javascript">
$('#fecha').val("<?php echo $ventaBoletos->fecha_emision_ven_eda ?>");
$('#total').val("<?php echo $ventaBoletos->total_ven_eda ?>");
$('#fk_id_tipo').val("<?php echo $ventaBoletos->fk_id_asi_eda  ?>");
$('#fk_pel').val("<?php echo $ventaBoletos->fk_id_pel_eda ?>");
$('#fk_id_dea_eda').val("<?php echo $ventaBoletos->fk_id_tar_eda  ?>");

</script>
<script>
function actualizar(){

  $.ajax({
    url:$("#frm_editar_boletos").prop("action"),
    data:$("#frm_editar_boletos").serialize(),
    type:"post",
    success:function(data){
      consultarBoletos();
      $("#modalEditarBoletos").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarBoletos").modal("hide");
          consultarBoletos();
      }else{
Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }

    }
  });
}
</script>
