<br>
<br>
<legend class="text-center">
  <h1> <b>GESTION DE VENTA BOLETOS</b> </h1>
</legend>
<br>
<!-- Button to Open the Modal -->
<center><button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#modalNuevaBoletos" class="text-center">
  Agregar nuevo
</button></center>

<!-- The Modal -->
<div class="modal" id="modalNuevaBoletos">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title " style="color:#000000">INGRESAR NUEVA VENTA DE BOLETOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm_nueva_boletos"class="" enctype="multipart/form-data" action="<?php echo site_url('boletos/guardarFunciones') ?>" method="post">
          <b>Fecha</b> <br>
          <div class="col-md-10">
            <input class="form-control" type="date" name="fecha_emision_ven_eda" id="fecha_emision_ven_eda" value="">
          </div>
          <br>
          <b>Total Boletos</b> <br>
          <div class="col-md-10">
            <input class="form-control" type="number" name="total_ven_eda" id="total_ven_eda" value="">

          </div>
          <br>
          <b>Asientos</b> <br>
          <div class="col-md-10">
             <select class="form-control" name="fk_id_asi_eda" id="fk_id_asi_eda" required data-live-search="true">
               <!-- cargar opciones de <select class="" name="">  </select> -->
               <option value="">-------------selecciones el Asientos-----------</option>
               <?php if ($listadoasientos): ?>
                 <?php foreach ($listadoasientos->result() as $asientoTemporal): ?>
                   <option value="<?php echo $asientoTemporal->id_asi_eda;?>">
                   <?php echo $asientoTemporal->fila_asi_eda; ?>
                   --
                   <?php echo $asientoTemporal->numero_asi_eda; ?>
                 <?php endforeach; ?>
               <?php endif; ?>
           </select>
           </div>
          <br>
          <b>Pelicula</b> <br>
          <div class="col-md-10">
             <select class="form-control" name="fk_id_pel_eda" id="fk_id_pel_eda" required data-live-search="true">
               <option value="">-------------selecciones el pelicula-----------</option>
               <?php if ($listadoPeliculas): ?>
                 <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
                   <option value="<?php echo $peliculaTemporal->id_pel_eda;?>">
                   <?php echo $peliculaTemporal->nombre_pel_eda; ?>
                   -
                   <?php echo $peliculaTemporal->tiempo_pel_eda; ?>
                   </option>
                 <?php endforeach; ?>
               <?php endif; ?>
           </select>
           </div>
          <br>
          <b>Tarifa</b> <br>
          <div class="col-md-10">

           <select class="form-control selectpicker"   name="fk_id_tar_eda" id="fk_id_tar_eda" required  data-live-search="true">
             <!-- cargar opciones de <select class="" name="">  </select> -->
             <option value="">-------------selecciones el Tarifa-----------</option>
             <?php if ($listadoTarifa): ?>
               <?php foreach ($listadoTarifa->result() as $tarifaTemporal): ?>
                 <option value="<?php echo $tarifaTemporal->id_tar_eda;?>">
                 <?php echo $tarifaTemporal->adulto_tar_eda; ?>
                 --
                 <?php echo $tarifaTemporal->estudiante_tar_eda; ?>
                 </option>
               <?php endforeach; ?>
             <?php endif; ?>
         </select>
         </div>
          <br>
          <button type="submit" name="button" class="btn btn-success"> <i class="glyphicon glyphicon-ok"></i> Guardar</button>
        </form>
      </div>
      <script type="text/javascript">
      $('#fk_id_asi_eda').selectpicker();
      $('#fk_id_pel_eda').selectpicker();
      </script>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>

<div class="container">
  <div class="" id="listado-boletos">

  </div>

</div>
<div class="modal" id="modalEditarBoletos">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

        <h4 class="modal-title" style="color:#000000">ACTUALIZACIÓN DE BOLETOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<br>


</script>
<script type="text/javascript">
function consultarBoletos(){
  $("#listado-boletos").html('<center><i class="fa fa-spinner fa-spin fa-3x "> </i><br> Espere por favor...</center>').load("<?php echo site_url('boletos/listado') ?>");


}
consultarBoletos(); //llamar a la funcion para que actualice la tabla automaticamente
</script>

<!-- <script type="text/javascript">
$("#fk_id_hor").selectpicker();

</script> -->
<script >

    function eliminar(id_ven_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("boletos/borrar"); ?>',
              data:{"id_ven_eda":id_ven_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalNuevaConfiguraciona").modal("hide");
                    consultarBoletos();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }


              }
            });


    }

</script>
<!-- validaciones-->
<script type="text/javascript">
   $("#frm_nueva_boletos").validate({
     rules:{
       fecha_emision_ven_eda:{
         required:true
       },
       total_ven_eda:{
         required: true,
         minlength:1,
         maxlength:10,
         digits:true
       },
       fk_id_pel_eda:{
         required:true
       },
       fk_id_sal:{
         required:true
       },
       fk_id_hor:{
         required:true

       }


     },
     messages:{
       fecha_emision_ven_eda:{
         required:"porfavor ingrese uana fecha "
       },
       total_ven_eda:{
         required:"porfavor ingrese un valor entero",
         minlength:"Número invalido por favor corrija",
         maxlength:"Número invalido por favor corrija",
         digits:"porfavor ingrese los numeros en el rrango de uno a 10"
       },
       fk_id_pel_eda:{
         required:"selecione una pelicula"
       },
       fk_id_sal:{
         required:"seleccione una sala "
       },
       fk_id_hor:{
         required:"seleccione una hora"
       }

     },


      submitHandler:function(formulario){
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url('boletos/guardarBoletos') ?>',
          data:$(formulario).serialize(),
          success:function(data){
            var objetoRespuesta=JSON.parse(data);
            if (objetoRespuesta.estado=="ok") {
              Swal.fire('confirmacion',objetoRespuesta.mensaje,'success');
                $("#modalNuevaBoletos").modal("hide");
                consultarBoletos();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>

<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('configuraciones/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('boletos/editar'); ?>/"+id);
      $("#modalEditarBoletos").modal("show");
      consultarBoletos();
    }
</script>
