<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalSalas">
   <i class="fa fa-plus" aria-hidden="true"></i> REGISTRAR NUEVA SALA <i class="fa fa-users" aria-hidden="true"></i>
</button></center>
<br><hr>
<legend class="text-center">
<i class="fa fa-users" aria-hidden="true"></i>
<b>GESTIÓN DE SALAS</b>
</legend>
<hr>


<!-- The Modal -->
<div class="modal" id="modalSalas">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"style="color:#000000">REGISTRO DE NUEVA SALA</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm-nueva-sala" class="" enctype="multipart/form-data" action="<?php echo site_url('salas/guardar'); ?>" method="post">
        <i class="fa fa-users" aria-hidden="true"></i>  <b>NOMBRE DE LA SALA </b>
          <br>
          <input type="text" name="nombre_sal_eda" value="" placeholder="Por favor Ingrese el nombre de la sala" class="form-control"><br>

            <button type="submit" name="button" class="btn btn-success"> <i class="glyphicon glyphicon-ok"></i> Guardar</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>
<!-- inicio contenedor -->
<div class="container">
  <div id="contenedor-listado-salas">

  </div>
</div>

<!-- inicio modal editar -->
<!-- The Modal -->
<div class="modal" id="modalEditarSala">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"style="color:#000000">ACTUALIZACIÓN  DE SALAS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<!-- inicio script consultar -->
<script type="text/javascript">
  function consultarSalas(){  // se carga la vista cuando se llama a la funcion
    $("#contenedor-listado-salas")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('salas/listado');?>");

  }
  consultarSalas();
</script>
<script >

    function eliminar(id_sal_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("salas/borrar"); ?>',
              data:{"id_sal_eda":id_sal_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalSalas").modal("hide");
                    consultarSalas();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }
              }
            });
    }

</script>

<script type="text/javascript">
  $("#frm-nueva-sala").validate({
    rules:{
      nombre_sal_eda:{
        required:true
      },
    },
    messages:{
      nombre_sal_eda:{
        required:"Por favor ingrese el nombre de la sala"
      },

  },

  submitHandler:function(formulario){
    var formData = new FormData($(formulario)[0]);
    $.ajax({
      type:'post',
      url:'<?php echo site_url("salas/guardar"); ?>',
      data:formData,
      contentType:false,
      processData:false,
      success:function(data){
        var objeto=JSON.parse(data);
        if (objeto.estado=="ok") {
          Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalSalas").modal("hide");
          consultarSalas();
        } else {
          Swal.fire('ERROR','Error al insertar. intente nuevamente','error');
        }
      }
    });
  }
  });
</script>

<script type="text/javascript">
  function cargarEdicion(id){
    $("#contenedor-edicion").load("<?php echo site_url('salas/editar'); ?>/"+id);
    $("#modalEditarSala").modal("show")
  }
  consultarSalas();
</script>
