<?php if ($listadoSala): ?>
  <table id="tbl-sala" class="table table-bordered table-striped table-hover" style="color:#ffffff">
    <thead>
      <tr>
        <th class="text-center">ID: </th>
        <th class="text-center"><i class="fa fa-users" aria-hidden="true"></i> NOMBRE DE DE LA SALA : </th>
        <th class="text-center">ACCIONES: </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoSala->result() as $salaTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $salaTemporal->id_sal_eda?></td>
          <td class="text-center"><?php echo $salaTemporal->nombre_sal_eda?></td>

          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $salaTemporal->id_sal_eda ?>);">
                  <i class="fa fa-pen"></i>

                </a>
                <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>
                <a onclick="eliminar(<?php echo $salaTemporal->id_sal_eda ?>);" class="btn btn-danger">
                              <i class="fa fa-trash"></i>
                            </a>
                            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="aler alert-danger">
    No se encontraron salas registrados.
  </div>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-sala").DataTable();
</script>
