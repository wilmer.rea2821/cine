<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_sala" enctype="multipart/form-data" class="" action="<?php echo site_url("salas/actualizar"); ?>" method="post">
  <input type="hidden" name="id_sal_eda" value="<?php echo $salaEditar->id_sal_eda; ?>">
<i class="fa fa-users" aria-hidden="true"></i>  <b>NOMBRE DE LA SALA</b>
  <br>
  <input type="text" id="nombre_sal_eda" name="nombre_sal_eda" value="<?php echo $salaEditar->nombre_sal_eda; ?>" placeholder="Ingrese la sala" class="form-control"> <br>

  <button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Actualizar
      </button>
    </form>
<script type="text/javascript">
function actualizar(){

  $.ajax({
    url:$("#frm_editar_sala").prop("action"),
    data:$("#frm_editar_sala").serialize(),
    type:"post",
    success:function(data){
      consultarSalas();
      $("#modalEditarSala").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarSala").modal("hide");
          consultarSalas();
      }else{
Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }

    }
  });
}
</script>
