<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>login</title>

    <script
  src="https://code.jquery.com/jquery-3.6.1.js"
  integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
  crossorigin="anonymous"></script>
  <link
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx"
    crossorigin="anonymous"
  />
<!-- inportacion de jquery validate  -->
<script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_AR.min.js'); ?>"></script>
<!-- inportacion de datatable -->
 <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css"> <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

<!-- inpórtavionde sweetalert -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- inportacion del select buscador -->
<!-- Importacion Bootstrap Select -->


  </head>
  <body class="bg-info d-flex justify-content-center align-items-center ">
    <br><br>
    <div class="col-md-12"> <br>
  <div class="row">
    <div class="col-md-4">

    </div>
    <div class="col-md-6">
      <div class="col-md-10  bg-white p-5 rounded-5  shadow"
      style="width: 25rem">
        <center>
          <div class="d-flex justify-content-center">
            <img  src="<?php echo base_url(); ?>/assets/images/login1.png" width="50%" alt="LOGO">
         </div>
          <h3>
            INICIO DE SESION
           </h3>
        </center>
        <br>
        <form  id="frm_login" method="post" action="<?php echo site_url('seguridades/validarUsuario'); ?>">

              <!-- Text input-->
              <div class="form-group">

                <label class="control-label" for="email_usu_eda">Email:</label> <br>
                <div class="col-md-12">
                <input id="email_usu_eda" name="email_usu_eda" type="text" placeholder="Ingrese su email" class="form-control input-md" required="">
                <span class="help-block">Ej. alex@gmail.com</span>
                </div>
              </div>
               <br>
              <!-- Password input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="password_usu_eda">Contraseña:</label>
                <div class="col-md-12">
                  <input id="password_usu_eda" name="password_usu_eda" type="password" placeholder="Ingrese su contraseña" class="form-control input-md" required="">

                </div>
              </div>

              <!-- Button (Double) -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
              <center>   <div class="col-md-8">
                 <button type="submit" class="btn btn-primary "> Ingresar</button>
                   <a class="btn btn-danger" href="<?php echo site_url('welcome'); ?>">Cancelar </a>  </center>
                </div>
              </div>


          </form>

      </div>


    </div>
  </div>

    <style>

        label.error{
            border:1px solid white;
            color:white;
            background-color:#E15B69;
            padding:5px;
            padding-left:15px;
            padding-right:15px;
            font-size:12px;
            opacity: 0;
              /visibility: hidden;/
              /position: absolute;/
              left: 0px;
              transform: translate(0, 10px);
              /background-color: white;/
              /padding: 1.5rem;/
              box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
              width: auto;
              margin-top:30px !important;


              z-index: 10;
              opacity: 1;
              visibility: visible;
              transform: translate(0, -20px);
              transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
              border-radius:10px;
              width:100%;

        }

        input.error{
            border:1px solid #E15B69;
        }


        select.error{
            border:1px solid #E15B69;
        }

        label.error:before{
             position: absolute;
              z-index: -1;
              content: "";
              right: calc(90% - 10px);
              top: -8px;
              border-style: solid;
              border-width: 0 10px 10px 10px;
              border-color: transparent transparent #E15B69 transparent;
              transition-duration: 0.3s;
              transition-property: transform;
        }


    </style>
    <?php if ($this->session->flashdata('confirmacion')): ?>
          <script type="text/javascript">
            $(document).ready(function(){
              Swal.fire(
                'CONFIRMACIÓN', //titulo
                '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
                'success' //Tipo de alerta
              )
            });
          </script>
        <?php endif; ?>


        <?php if ($this->session->flashdata('error')): ?>
          <script type="text/javascript">
            $(document).ready(function(){
              Swal.fire(
                'ERROR', //titulo
                '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
                'error' //Tipo de alerta
              )
            });
          </script>
        <?php endif; ?>


  </body>
</html>
