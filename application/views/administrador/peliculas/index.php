<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalNuevaPelicula">
   <i class="glyphicon glyphicon-home"></i>REGISTRAR PELICULA
</button></center>
<br><hr>
<legend class="text-center">
<i class="glyphicon glyphicon-plus"></i>
<b>GESTIÓN DE PELICULAS</b>
</legend>
<hr>

<!-- The Modal -->
<div class="modal" id="modalNuevaPelicula">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="color:#000000">REGISTRO DE ASIENTOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form  id="frm_nueva_pelicula" enctype="multipart/form-data" class="" action="<?php echo site_url("peliculas/guardar"); ?>" method="post">
          <i class="fas fa-video"></i><b> NOMBRE DE LA PELICULA</b> <i class="	glyphicon glyphicon-facetime-video"></i>
          <br>
          <input type="text" id="nombre_pel_eda" name="nombre_pel_eda" value="" placeholder="Ingrese la restriccion" class="form-control"> <br>
          <i class="fas fa-surprise"></i><b> TIPO DE LA PELICULA</b>
          <br>
          <input type="text" id="tipo_pel_eda" name="tipo_pel_eda" value="" placeholder="Ingrese tipo de pelicula" class="form-control"> <br>
          <i class='fas fa-business-time'></i><b>TIEMPO DE LA PELICULA </b>
          <br>
          <input type="numbre" id="tiempo_pel_eda" name="tiempo_pel_eda" value="" placeholder="Ingrese el tiempo" class="form-control"> <br>
          <i class="fas fa-book-reader"></i><b> DETALLE DE LA PELICULA</b>
          <br>
          <input type="text" id="detalle_pel_eda" name="detalle_pel_eda" value="" placeholder="Ingrese detalle " class="form-control"> <br>
          <i class="fas fa-times"></i><b> RESTRICCION DE LA PELICULA</b>
          <br>
          <input type="text" id="restriccion_pel_eda" name="restriccion_pel_eda" value="" placeholder="Ingrese la restriccion" class="form-control"> <br>
          <i class="fa fa-camera" aria-hidden="true"></i> <b>FOTO</b>
          <br>
          <input type="file" id="foto_pel_eda" name="foto_pel_eda" value="" class="form-control" placeholder="Ingrese su foto"  accept="image/*">
          <button type="submit"class="btn btn-success" name="button">guardar</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<div class="container">
  <div id="contenedor-listado-pelicula">
    <!-- fa-lg(tamaño)/fa-6x(tamaño)/ fa-spin(efecto de rotar)-->
  </div>
</div>



<div class="modal" id="modalEditarPelicula">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

        <h4 class="modal-title" style="color:#000000">ACTUALIZACIÓN DE PELICULAS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<!-- inicio script funcion consultarProfesores id(#) clase(.) -->
<script type="text/javascript">
  function consultarpelicula(){  // se carga la vista cuando se llama a la funcion
    $("#contenedor-listado-pelicula")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('peliculas/listado');?>");

  }
  consultarpelicula();
</script>
<script >

    function eliminar(id_pel_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("peliculas/borrar"); ?>',
              data:{"id_pel_eda":id_pel_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalNuevaPelicula").modal("hide");
                    consultarpelicula();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }


              }
            });


    }

</script>
<script type="text/javascript">
   $("#frm_nueva_pelicula").validate({
      rules:{
          nombre_pel_eda:{
            required:true,
            minlength:3
          },
          tipo_pel_eda:{
            required: true,
            minlength:3
          },
          tiempo_pel_eda:{
            required: true,
            minlength:2
          },
          detalle_pel_eda:{
            required:true,
            minlength:5

        },
        restriccion_pel_eda:{
          required: true,
          minlength:2
        }
      },
      messages:{
        nombre_pel_eda:{
          required:"Por favor ingrese el nombre de la empresa",
          minlength:"Por favor ingrese un nombre valido"
        },
        tipo_pel_eda:{
          required: "Por favor ingrrese el tipo de pelicula",
          minlength:"Por favor ingrese un tipo de pelicula valido"
        },
        tiempo_pel_eda:{
          required: "Por favor ingrese el tiempo de la pelicula",
          minlength:"Por favor ingrese un tiempo valido"
        },
        detalle_pel_eda:{
          required:"Por favor ingrese el detalla de la pelicula",
          minlength:"Por favor ingrese detalle valido"
        },

      restriccion_pel_eda:{
        required:"Por favor ingrese la restriccion de la pelicula",
        minlength:"Por favor ingrese restriccion valida"
      }

    },
      submitHandler:function(formulario){

        var formData = new FormData($(formulario)[0]);
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url("peliculas/guardar"); ?>',
          data:formData,
          contentType: false,
          processData: false,
          success:function(data){
            var objeto=JSON.parse(data);
            if (objeto.estado=="ok") {
              Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                $("#modalNuevaPelicula").modal("hide");
                consultarpelicula();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>


<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('asientos/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('peliculas/editar'); ?>/"+id);
      $("#modalEditarPelicula").modal("show");
      consultarpelicula();
    }
</script>
