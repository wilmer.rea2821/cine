<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_asiento" enctype="multipart/form-data" class="" action="<?php echo site_url("peliculas/actualizar"); ?>" method="post">
  <input type="hidden" name="id_pel_eda" value="<?php echo $pelEditar->id_pel_eda; ?>">
  <i class="fas fa-video"></i><b> NOMBRE DE LA PELICULA</b>
  <br>
  <input type="text" id="nombre_pel_eda" name="nombre_pel_eda" value="<?php echo $pelEditar-> nombre_pel_eda?>" placeholder="Ingrese la pelicula" class="form-control"> <br>
  <i class="fas fa-surprise"></i><b> TIPO DE LA PELICULA</b>
  <br>
            <input type="text" id="tipo_pel_eda" name="tipo_pel_eda" value="<?php echo $pelEditar-> tipo_pel_eda?>" placeholder="Ingrese tipo de pelicula" class="form-control"> <br>
  <i class='fas fa-business-time'></i><b>TIEMPO DE LA PELICULA </b>
            <br>
            <input type="numbre" id="tiempo_pel_eda" name="tiempo_pel_eda" value="<?php echo $pelEditar-> tiempo_pel_eda?>" placeholder="Ingrese el tiempo" class="form-control"> <br>
  <i class="fas fa-book-reader"></i><b> DETALLE DE LA PELICULA</b>
            <br>
            <input type="text" id="detalle_pel_eda" name="detalle_pel_eda" value="<?php echo $pelEditar-> detalle_pel_eda?>" placeholder="Ingrese detalle " class="form-control"> <br>
  <i class="fas fa-times"></i><b> RESTRICCION DE LA PELICULA</b>
            <br>
            <input type="text" id="restriccion_pel_eda" name="restriccion_pel_eda" value="<?php echo $pelEditar-> restriccion_pel_eda?>" placeholder="Ingrese la restriccion" class="form-control"> <br>
  <button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-pen"></i> Actualizar
      </button>
    </form>
<script type="text/javascript">
function actualizar(){

  $.ajax({
    url:$("#frm_editar_asiento").prop("action"),
    data:$("#frm_editar_asiento").serialize(),
    type:"post",
    success:function(data){
      consultarpelicula();
      $("#modalEditarPelicula").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarPelicula").modal("hide");
          consultarpelicula();
      }else{
Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }

    }
  });
}
</script>
