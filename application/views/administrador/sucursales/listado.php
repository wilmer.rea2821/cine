<?php if ($sucursales): ?>
  <!-- tabla  -->
  <table id="tbl-sucursales" class="table table-bordered table-striped table-hover" id="tbl-profesores">
    <thead>
      <tr>
        <th>ID</th>
        <th>PROVINCIA</th>
        <th>CIUDAD</th>
        <th>ESTADO</th>
        <th>DIRECCION</th>
        <th>EMAIL</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($sucursales->result() as $sucursal ): ?>
        <tr>
          <td> <?php echo $sucursal->id_suc_eda ?></td>
          <td> <?php echo $sucursal->provincia_suc_eda ?></td>
          <td> <?php echo $sucursal->ciudad_suc_eda ?></td>
          <td> <?php echo $sucursal->estado_suc_eda ?></td>
          <td> <?php echo $sucursal->direccion_suc_eda ?></td>
          <td> <?php echo $sucursal->email_suc_eda ?></td>
          <td>
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $sucursal->id_suc_eda ?>);">
                  <i class="fa fa-pen"></i>

            </a>
            <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>
            <a onclick="eliminar(<?php echo $sucursal->id_suc_eda ?>);" class="btn btn-danger">
              <i class="fa fa-trash"></i>
             <?php endif; ?>
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>


  </table>

<?php else: ?>
  <div class="alert alert-danger">
    No se encuentra sucursales registrados.

  </div>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-sucursales").DataTable();
</script>
