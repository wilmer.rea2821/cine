<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_sucursal" enctype="multipart/form-data" class="" action="<?php echo site_url("sucursales/actualizar"); ?>" method="post">
  <input type="hidden" name="id_suc_eda" value="<?php echo $sucursalEditar->id_suc_eda; ?>">
  <b>PPROVINCIA:</b>
  <br>
  <input type="text" id="provincia_suc_eda" name="provincia_suc_eda" value="<?php echo $sucursalEditar->provincia_suc_eda; ?>" placeholder="Ingrese la provincia" class="form-control">
  <br>
  <b>CIUDAD:</b>
  <br>
  <input type="text" id="ciudad_suc_eda" name="ciudad_suc_eda" value="<?php echo $sucursalEditar->ciudad_suc_eda; ?>" placeholder="Ingrese la ciudad" class="form-control">
  <br>
  <b>ESTADO:</b>
  <br>
  <select class="form-control" name="estado_suc_eda" id=estado_suc_eda"">
      <option value= " ">--Seleccione una opcion</option>
      <option value="Activo">Activo</option>
      <option value="Inactivo">Inactivo</option>
  </select>
  <br>
  <b>DIRECCIÓN:</b>
  <br>
  <input type="text" id="direccion_suc_eda" name="direccion_suc_eda" value="<?php echo $sucursalEditar->direccion_suc_eda; ?>" placeholder="Ingrese la direeccion" class="form-control">
  <br>
  <b>E-MAIL:</b>
  <br>
  <input type="text" id="email_suc_eda" name="email_suc_eda" value="<?php echo $sucursalEditar->email_suc_eda; ?>" class="form-control" placeholder="Ingrese el e-mail">

  <button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Actualizar
      </button>
    </form>
<script type="text/javascript">
function actualizar(){

  $.ajax({
    url:$("#frm_editar_sucursal").prop("action"),
    data:$("#frm_editar_sucursal").serialize(),
    type:"post",
    success:function(data){
      consultarSucursales();
      $("#modalEditarSucursal").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarSucursal").modal("hide");
          consultarSucursales();
      }else{
      Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }

    }
  });
}
</script>

<script type="text/javascript">
  //abriendo javascript proceso para modificar datos select

    $('#estado_suc_eda').val("<?php echo $sucursalEditar->estado_suc_eda; ?>");
    </script>
