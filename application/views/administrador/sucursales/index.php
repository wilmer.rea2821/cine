
<br>
<center> <h1> <b> GESTIÓN DE SUCURSALES </b> </h1> </center>


<!-- Trigger the modal with a button -->
 <br> <br>

<!-- Button to Open the Modal -->
<center>
  <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalSucursal">
    REGISTRO DE SUCURSALES
  </button>
</center>


<!-- The Modal -->
<div class="modal" id="modalSucursal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="color:#000000">INGRESO DE SUCURSALES</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm_nuevo_sucursal" class="" action=" <?php echo site_url('sucursales/guardar') ?>" method="post">

          <b>PPROVINCIA:</b>
          <br>
          <input type="text" id="provincia_suc_eda" name="provincia_suc_eda" value="" placeholder="Ingrese la provincia" class="form-control">
          <br>
          <b>CIUDAD:</b>
          <br>
          <input type="text" id="ciudad_suc_eda" name="ciudad_suc_eda" value="" placeholder="Ingrese la ciudad" class="form-control">
          <br>
          <b>ESTADO:</b>
          <br>
          <select class="form-control" name="estado_suc_eda" id=estado_suc_eda"">
              <option value= " ">--Seleccione una opcion</option>
              <option value="Activo">Activo</option>
              <option value="Inactivo">Inactivo</option>
          </select>
          <br>
          <b>DIRECCIÓN:</b>
          <br>
          <input type="text" id="direccion_suc_eda" name="direccion_suc_eda" value="" placeholder="Ingrese la direeccion" class="form-control">
          <br>
          <b>E-MAIL:</b>
          <br>
          <input type="text" id="email_suc_eda" name="email_suc_eda" value="" class="form-control" placeholder="Ingrese el e-mail">
          <br>
          <button   type="submit" name="button" class="btn btn-success">
            <i class="glyphicon glyphicon-ok" ></i>
            GUARDAR
          </button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">CANCELAR</button>
      </div>

    </div>
  </div>
</div>
<!-- <button type="button" name="button"onclick="consultarProfesores()"> Actualizar Datos</button> -->
<div class="container">
  <div id="contenedor-listado-sucursal">
  <!-- <center>  <i class="fa fa-spinner fa-5x fa-spin"></i> <br>Espere por favor</center> -->

  </div>
</div>

<div class="modal" id="modalEditarSucursal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"style="color:#000000">ACTUALIZACIÓN  DE SUCURSALES</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  function consultarSucursales(){
    $("#contenedor-listado-sucursal ").html('<center>  <i class="fa fa-spinner fa-5x fa-spin"></i> <br>Espere por favor....</center>')
    .load("<?php echo site_url('sucursales/listado') ?>"); // cargamos una vista dentro del contenedor atrves de su id / se hace un solo archivo el encabezado contenido y pie
  }
  consultarSucursales(); //llamamos a la funcion para cargar autoamtica
</script>
<script >

    function eliminar(id_suc_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("sucursales/borrar"); ?>',
              data:{"id_suc_eda":id_suc_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalSucursal").modal("hide");
                    consultarSucursales();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }
              }
            });
    }

</script>

<script type="text/javascript">
   $("#frm_nuevo_sucursal").validate({
      rules:{
          provincia_suc_eda:{
            required:true
          },
          ciudad_suc_eda:{
            required: true

          },
          estado_suc_eda:{
            required: true

          },
          direccion_suc_eda:{
            required: true

          },
          email_suc_eda:{
            required: true

          }
      },
      messages:{
        provincia_suc_eda:{
          required:"Por favor ingrese la provincia"
        },
        ciudad_suc_eda:{
          required: "Por favor ingrese la ciudad"

        },
        estado_suc_eda:{
          required: "Por favor ingrese el estado"

        },
        direccion_suc_eda:{
          required: "Por favor ingrese la direccion"

        },
        email_suc_eda:{
          required: "Por favor ingrese su correo electronico"

        }
      },
      submitHandler:function(formulario){
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url("sucursales/guardar"); ?>',
          data:$(formulario).serialize(),
          success:function(data){
            var objetoRespuesta=JSON.parse(data);
            if (objetoRespuesta.estado=="ok") {
              Swal.fire('confirmacion',objetoRespuesta.mensaje,'success');
                $("#modalSucursal").modal("hide");
                consultarSucursales(); // actualizar los datos luego de presionar en el boton gruardar llamamos a la funcion
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>

<script type="text/javascript">
  function cargarEdicion(id){
    $("#contenedor-edicion").load("<?php echo site_url('sucursales/editar'); ?>/"+id);
    $("#modalEditarSucursal").modal("show")
  }
  consultarSucursales();
</script>
