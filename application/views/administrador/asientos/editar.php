<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_asiento" enctype="multipart/form-data" class="" action="<?php echo site_url("asientos/actualizar"); ?>" method="post">
  <input type="hidden" name="ind_asi_eda" value="<?php echo $asientoEditar->id_asi_eda; ?>">
  <i class="fas fa-couch"></i><b>FILA</b> <br>
  <select class="form-control selectpicker" name="fila_asi_eda" id="fila_asi" required data-live-search="true">
    <option value="">-------seleccione una fila---------</option>
    <option class="text-center" value="A">A</option>
    <option class="text-center" value="B">B</option>
    <option class="text-center" value="C">C</option>
    <option class="text-center" value="D">D</option>
    <option class="text-center" value="E">E</option>
    <option class="text-center" value="F">F</option>
    <option class="text-center" value="G">G</option>
    <option class="text-center" value="H">H</option>
    <option class="text-center" value="I">I</option>
    <option class="text-center" value="J">J</option>
  </select>
  <br>
  <i class="fas fa-couch"></i><b>NÚMERO DE ASIENTO</b> <br>
  <select class="form-control selectpicker" name="numero_asi_eda" id="fila_asis" required data-live-search="true">
    <option value="">-------seleccione una fila---------</option>
    <option class="text-center" value="1">1</option>
    <option class="text-center" value="2">2</option>
    <option class="text-center" value="3">3</option>
    <option class="text-center" value="4">4</option>
    <option class="text-center" value="5">5</option>
    <option class="text-center" value="6">6</option>
    <option class="text-center" value="7">7</option>
    <option class="text-center" value="8">8</option>
    <option class="text-center" value="9">9</option>
    <option class="text-center" value="10">10</option>
    <option class="text-center" value="11">11</option>
  </select>
  <br>
  <center><button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Actualizar
      </button></center>

    </form>

    <script type="text/javascript">
    function actualizar(){

      $.ajax({
        url:$("#frm_editar_asiento").prop("action"),
        data:$("#frm_editar_asiento").serialize(),
        type:"post",
        success:function(data){
          consultarasientos();
          $("#modalEditarAsiento").modal("hide");
          $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
          $('.modal-backdrop').remove();//eliminamos el backdrop del modal
          var objeto=JSON.parse(data);
          if(objeto.estado=="ok" || objeto.respuesta=="OK"){
            Swal.fire('CONFIRMACION',objeto.mensaje,'success');
              $("#modalEditarAsiento").modal("hide");
              consultarasientos();
              redirect('asientos/index');
          }else{
    Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
          }

        }
      });
    }
    </script>

    <script type="text/javascript">
    $("#fila_asi").val("<?php echo $asientoEditar->fila_asi_eda?>");
    $("#fila_asis").val("<?php echo $asientoEditar->numero_asi_eda?>");
    </script>
