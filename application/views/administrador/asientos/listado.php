<?php if ($asientos): ?>
  <table id="tbl-asientos" class="table table-bordered table-striped table-hover" style="color:#ffffff">
    <thead>
      <tr>
        <th class="text-center"><i class="fas fa-id-card"></i>ID: </th>
        <th class="text-center"><i class="fas fa-couch"></i>FILA : </th>
        <th class="text-center"><i class="fas fa-couch"></i>NUMERO DE ASIENTO: </th>
        <th class="text-center">ACCIONES: </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($asientos->result() as $asientoTemporal): ?>
        <tr>
          <td class="text-center" ><?php echo $asientoTemporal->id_asi_eda?></td>
          <td class="text-center" ><?php echo $asientoTemporal->fila_asi_eda?></td>
          <td class="text-center" ><?php echo $asientoTemporal->numero_asi_eda?></td>
          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $asientoTemporal->id_asi_eda ?>);">
                  <i class="fa fa-pen"></i>
                </a>
                <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>

            <a onclick="eliminar(<?php echo $asientoTemporal->id_asi_eda ?>);" class="btn btn-danger">
            <i class="fa fa-trash"></i>
            </a>
              <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="aler alert-danger">
    No se encontraron asientos registrados.
  </div>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-asientos").DataTable();
</script>
