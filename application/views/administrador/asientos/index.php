<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalNuevoAsiento">
   <i class="glyphicon glyphicon-home"></i>REGISTRAR ASIENTOS
</button></center>
<br><hr>
<legend class="text-center">
<i class="glyphicon glyphicon-plus"></i>
<b>GESTIÓN DE ASIENTOS</b>
</legend>
<hr>

<!-- The Modal -->
<div class="modal" id="modalNuevoAsiento">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="color:#000000">REGISTRO DE ASIENTOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form  id="frm_nuevo_asiento" enctype="multipart/form-data" class="" action="<?php echo site_url("asientos/guardar"); ?>" method="post">
          <i class="fas fa-couch"></i><b>FILA</b> <br>
          <select class="form-control text-center" name="fila_asi_eda" id="fila_asi_eda" required >
            <option value="">-------seleccione una fila---------</option>
            <option value="A">A</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="D">D</option>
            <option value="E">E</option>
            <option value="F">F</option>
            <option value="G">G</option>
            <option value="H">H</option>
            <option value="I">I</option>
            <option value="J">J</option>
          </select>
          <br>
          <i class="fas fa-couch"></i><b>NÚMERO DE ASIENTO</b> <br>
          <select class="form-control text-center" name="numero_asi_eda" id="numero_asi_eda" required >
            <option value="">-------seleccione una número de asiento---------</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
          </select>
          <br>
          <button type="submit"class="btn btn-success" name="button">guardar</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<div class="container">
  <div id="contenedor-listado-asiento">
    <!-- fa-lg(tamaño)/fa-6x(tamaño)/ fa-spin(efecto de rotar)-->
  </div>
</div>



<div class="modal" id="modalEditarAsiento">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

        <h4 class="modal-title" style="color:#000000">ACTUALIZACIÓN DE ASIENTOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<!-- inicio script funcion consultarProfesores id(#) clase(.) -->
<script type="text/javascript">
  function consultarasientos(){  // se carga la vista cuando se llama a la funcion
    $("#contenedor-listado-asiento")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('asientos/listado');?>");

  }
  consultarasientos();
</script>
<script >

    function eliminar(id_asi_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("asientos/borrar"); ?>',
              data:{"id_asi_eda":id_asi_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalNuevoAsiento").modal("hide");
                    consultarasientos();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }


              }
            });


    }

</script>
<script type="text/javascript">
   $("#frm_nuevo_asiento").validate({
      rules:{
          fila_asi_eda:{
            required:true
          },
          numero_asi_eda:{
            required: true
        }
      },
      messages:{
        fila_asi_eda:{
          required:"Por favor ingrese la fila de su asiento"
        },
        numero_asi_eda:{
          required: "Por favor ingrrese el numero de su asiento"
      }

    },
      submitHandler:function(formulario){

        var formData = new FormData($(formulario)[0]);
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url("asientos/guardar"); ?>',
          data:formData,
          contentType: false,
          processData: false,
          success:function(data){
            var objeto=JSON.parse(data);
            if (objeto.estado=="ok") {
              Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                $("#modalNuevoAsiento").modal("hide");
                consultarasientos();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>


<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('asientos/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('asientos/editar'); ?>/"+id);
      $("#modalEditarAsiento").modal("show");
      consultarasientos();
    }
</script>
