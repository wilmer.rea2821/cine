<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_configuracion" enctype="multipart/form-data" class="" action="<?php echo site_url("configuraciones/actualizar"); ?>" method="post">
  <input type="hidden" name="id_con_eda" value="<?php echo $configuracionEditar->id_con_eda; ?>">
  <i class="fa fa-building" aria-hidden="true"></i> <b>NOMBRE DE LA EMPRESA</b>
  <br>
  <input type="text" id="nombre_empresa_con_eda_e" name="nombre_empresa_con_eda" value="<?php echo $configuracionEditar->nombre_empresa_con_eda; ?>" placeholder="Ingrese el nombre" class="form-control"> <br>
<i class="fa fa-list-ol" aria-hidden="true"></i>  <b>RUC DE LA EMPRESA</b>
  <br>
  <input type="number" id="ruc_con_eda_e" name="ruc_con_eda" value="<?php echo $configuracionEditar->ruc_con_eda; ?>" placeholder="Ingrese el ruc" class="form-control"> <br>
<i class="fa fa-phone-square" aria-hidden="true"></i>  <b>TELEFONO DE LA EMPRESA</b>
  <br>
  <input type="number" id="telefono_con_eda_e" name="telefono_con_eda" value="<?php echo $configuracionEditar->telefono_con_eda; ?>" placeholder="Ingrese el telefono" class="form-control"> <br>
  <i class="fa fa-location-arrow" aria-hidden="true"></i><b>DIRECCIÓN DE LA EMPRESA</b>
  <br>
  <input type="text" id="direccion_con_eda_e" name="direccion_con_eda" value="<?php echo $configuracionEditar->direccion_con_eda; ?>" placeholder="Ingrese direccion" class="form-control"> <br>
  <i class="fa fa-user-circle" aria-hidden="true"></i> <b>REPRESENTANTE LEGAL DE LA EMPRESA</b>
  <br>
  <input type="text" required id="representante_con_eda_e" name="representante_con_eda" value="<?php echo $configuracionEditar->representante_con_eda; ?>" placeholder="Ingrese representante" class="form-control"> <br>

  <button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Actualizar
      </button>
    </form>
<script type="text/javascript">
function actualizar(){

  $.ajax({
    url:$("#frm_editar_configuracion").prop("action"),
    data:$("#frm_editar_configuracion").serialize(),
    type:"post",
    success:function(data){
      consultarConfiguraciones();
      $("#modalEditarConfiguraciona").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarConfiguraciona").modal("hide");
          consultarConfiguraciones();
      }else{
Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }

    }
  });
}
</script>
