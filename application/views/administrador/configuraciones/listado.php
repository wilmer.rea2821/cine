<?php if ($configuraciones): ?>
  <table id="tbl-configuraciones" class="table table-bordered table-striped table-hover" style="color:#ffffff">
    <thead>
      <tr>
        <th class="text-center"> <i class="fa fa-id-badge" aria-hidden="true"></i> ID: </th>
        <th class="text-center"> <i class="fa fa-building" aria-hidden="true"></i> NOMBRE EMPRESA: </th>
        <th class="text-center"><i class="fa fa-list-ol" aria-hidden="true"></i> RUC: </th>
        <th class="text-center"><i class="fa fa-phone-square" aria-hidden="true"></i> TELEFONO: </th>
        <th class="text-center"><i class="fa fa-location-arrow" aria-hidden="true"></i> DIRECCION: </th>
        <th class="text-center"><i class="fa fa-user-circle" aria-hidden="true"></i> REPRESENTANTE: </th>
        <th class="text-center"><i class="fa fa-camera" aria-hidden="true"></i> FOTO: </th>
        <th class="text-center">ACCIONES: </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($configuraciones->result() as $configuracion): ?>
        <tr>
          <td><?php echo $configuracion->id_con_eda ?></td>
          <td><?php echo $configuracion->nombre_empresa_con_eda ?></td>
          <td><?php echo $configuracion->ruc_con_eda?></td>
          <td><?php echo $configuracion->telefono_con_eda ?></td>
          <td><?php echo $configuracion->direccion_con_eda ?></td>
          <td><?php echo $configuracion->representante_con_eda ?></td>
          <td class="text-center"><?php if ($configuracion->foto_con_eda!=""): ?>
                    <a href="<?php echo base_url('uploads/config').'/'.$configuracion->foto_con_eda; ?>"
                      target="_blank">
                      <img src="<?php echo base_url('uploads/config').'/'.$configuracion->foto_con_eda; ?>"
                      width="50px" height="50px"
                      alt="">
                    </a>
                  <?php else: ?>
                    N/A
                <?php endif; ?></td>
          <td>
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $configuracion->id_con_eda ?>);">
                  <i class="fa fa-pen"></i>
                </a>
                <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>

            <a onclick="eliminar(<?php echo $configuracion->id_con_eda ?>);" class="btn btn-danger">
              <i class="fa fa-trash"></i>
              <?php endif; ?>

            </a>
          </td>
        </tr>
        <script type="text/javascript">
        $("#tbl-configuraciones").DataTable();
        </script>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="aler alert-danger">
    No se encontraron configuraciones registrados.
  </div>
<?php endif; ?>
