<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalNuevaConfiguraciona">
  <i class="fa fa-building" aria-hidden="true"></i> REGISTRAR EMPRESA
</button></center>
<br><hr>
<legend class="text-center" style="color:#ffffff">
<i class="glyphicon glyphicon-plus"></i>
 <i class="fa fa-building" aria-hidden="true"></i> <b>GESTIÓN DE CONFIGURACIÓN</b>
</legend>
<hr>

<!-- The Modal -->
<div class="modal" id="modalNuevaConfiguraciona">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="color:#000000">REGISTRO DE CONFIGURACIONES</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form  id="frm_nueva_configuracion" enctype="multipart/form-data" class="" action="<?php echo site_url("configuraciones/guardar"); ?>" method="post">
        <i class="fa fa-building" aria-hidden="true"></i> <b>NOMBRE DE LA EMPRESA</b>
          <br>
          <input type="text" id="nombre_empresa_con_eda" name="nombre_empresa_con_eda" value="" placeholder="Ingrese el nombre" class="form-control"> <br>
        <i class="fa fa-list-ol" aria-hidden="true"></i>  <b>RUC DE LA EMPRESA</b>
          <br>
          <input type="number" id="ruc_con_eda" name="ruc_con_eda" value="" placeholder="Ingrese el ruc" class="form-control"> <br>
        <i class="fa fa-phone-square" aria-hidden="true"></i>  <b>TELEFONO DE LA EMPRESA</b>
          <br>
          <input type="number" id="telefono_con_eda" name="telefono_con_eda" value="" placeholder="Ingrese el telefono" class="form-control"> <br>
          <i class="fa fa-location-arrow" aria-hidden="true"></i> <b>DIRECCIÓN DE LA EMPRESA</b>
          <br>
          <input type="text" id="direccion_con_eda" name="direccion_con_eda" value="" placeholder="Ingrese direccion" class="form-control"> <br>
          <i class="fa fa-user-circle" aria-hidden="true"></i> <b>REPRESENTANTE LEGAL DE LA EMPRESA</b>
          <br>
          <input type="text" id="representante_con_eda" name="representante_con_eda" value="" placeholder="Ingrese representante" class="form-control"> <br>
          <i class="fa fa-camera" aria-hidden="true"></i> <b>FOTO</b>
          <br>
          <input type="file" id="foto_con_eda" name="foto_con_eda" value="" class="form-control" placeholder="Ingrese su foto"  accept="image/*">
          <button type="submit"class="btn btn-success" name="button">guardar</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<div class="container">
  <div id="contenedor-listado-configuracion">
    <!-- fa-lg(tamaño)/fa-6x(tamaño)/ fa-spin(efecto de rotar)-->
  </div>
</div>



<div class="modal" id="modalEditarConfiguraciona">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

        <h4 class="modal-title" style="color:#000000">ACTUALIZACIÓN DE CONFIGURACIONES</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<!-- inicio script funcion consultarProfesores id(#) clase(.) -->
<script type="text/javascript">
  function consultarConfiguraciones(){  // se carga la vista cuando se llama a la funcion
    $("#contenedor-listado-configuracion")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('configuraciones/listado');?>");

  }
  consultarConfiguraciones();
</script>
<script >

    function eliminar(id_con_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("configuraciones/borrar"); ?>',
              data:{"id_con_eda":id_con_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalNuevaConfiguraciona").modal("hide");
                    consultarConfiguraciones();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }


              }
            });


    }

</script>
<script type="text/javascript">
   $("#frm_nueva_configuracion").validate({
      rules:{
          nombre_empresa_con_eda:{
            required:true
          },
          ruc_con_eda:{
            required: true,
            minlength:13,
            maxlength:13,
            digits:true
          },
          telefono_con_eda:{
            required: true,
            minlength:10,
            maxlength:10,
             digits:true
          },
          direccion_con_eda:{
            required: true

        },
        representante_con_eda:{
          required: true

      },
      foto_con_eda:{
        required: true
      }
      },
      messages:{
        nombre_empresa_con_eda:{
          required:"Por favor ingrese el nombre de la empresa"
        },
        ruc_con_eda:{
          required: "Por favor ingrrese el ruc de la empresa",
          minlength:"Ruc Incorrecto",
          maxlength:"Ruc Incorrecto",
          digits:"porfavor ingrese solo numeros"
        },
        telefono_con_eda:{
          required: "Telefono incorrecto por favor ingrese un número valido",
          minlength:"Celular Incorrecto",
          maxlength:"Celular Incorrecto",
          digits:"porfavor ingrese solo numeros"
        },
        direccion_con_eda:{
          required:"Por favor ingrese la direccion"
        },

      representante_con_eda:{
        required:"Por favor ingrese el representante de la empresa"
      },
      foto_con_eda:{
        required:"Por favor seleciona una foto"
      }

    },
      submitHandler:function(formulario){

        var formData = new FormData($(formulario)[0]);
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url("configuraciones/guardar"); ?>',
          data:formData,
          contentType: false,
          processData: false,
          success:function(data){
            var objeto=JSON.parse(data);
            if (objeto.estado=="ok") {
              Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                $("#modalNuevaConfiguraciona").modal("hide");
                consultarConfiguraciones();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>


<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('configuraciones/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('configuraciones/editar'); ?>/"+id);
      $("#modalEditarConfiguraciona").modal("show");
      consultarConfiguraciones();
    }
</script>
