<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_hora" enctype="multipart/form-data" class="" action="<?php echo site_url("tiempos/actualizar"); ?>" method="post">
  <input type="hidden" name="id_hor_eda" value="<?php echo $tiempoEditar->id_hor_eda; ?>">
  <i class="fa fa-calendar" aria-hidden="true"></i> <b>Actualizar hora</b>
  <br>
  <input type="time" id="hora_hor_eda" name="hora_hor_eda" value="<?php echo $tiempoEditar->hora_hor_eda; ?>" placeholder="Ingrese la fila de asiento" class="form-control"> <br>
<i class="fa fa-calendar" aria-hidden="true"></i>  <b>Actualizar fecha</b>
  <br>
  <input type="date" id="fecha_hor_eda" name="fecha_hor_eda" value="<?php echo $tiempoEditar->fecha_hor_eda; ?>" placeholder="Ingrese la fila de asiento" class="form-control"> <br>

  <button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Actualizar
      </button>
    </form>
<script type="text/javascript">
function actualizar(){

  $.ajax({
    url:$("#frm_editar_hora").prop("action"),
    data:$("#frm_editar_hora").serialize(),
    type:"post",
    success:function(data){
      consultarHoras();
      $("#modalEditarHora").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarHora").modal("hide");
          consultarHoras();
      }else{
Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }

    }
  });
}
</script>
