<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalHoras">
   <i class="fa fa-plus" aria-hidden="true"></i> REGISTRAR NUEVO HORARIO
</button></center>
<br><hr>
<legend class="text-center">
<i class="fa fa-calendar" aria-hidden="true"></i>
<b>GESTIÓN DE HORARIO</b>
</legend>
<hr>


<!-- The Modal -->
<div class="modal" id="modalHoras">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"style="color:#000000">REGISTRO DE NUEVO HORARIO</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm-nueva-hora" class="" enctype="multipart/form-data" action="<?php echo site_url('tiempos/guardar'); ?>" method="post">
        <i class="fa fa-calendar" aria-hidden="true"></i>  <b>INGRESAR LA HORA </b>
          <br>
          <input type="time" id="hora_hor_eda" name="hora_hor_eda" value="" placeholder="Por favor Ingrese la hora" class="form-control"><br>
        <i class="fa fa-calendar" aria-hidden="true"></i>  <b>INGRESAR LA FECHA </b>
          <br>
          <input type="date" name="fecha_hor_eda" value="" placeholder="Por favor Ingrese la fecha" class="form-control"><br>

            <button type="submit" name="button" class="btn btn-success"> <i class="glyphicon glyphicon-ok"></i> Guardar</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>
<!-- inicio contenedor -->
<div class="container">
  <div id="contenedor-listado-horas">

  </div>
</div>

<!-- inicio modal editar -->
<!-- The Modal -->
<div class="modal" id="modalEditarHora">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"style="color:#000000">ACTUALIZACIÓN  DE HORARIO</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<!-- inicio script consultar -->
<script type="text/javascript">
  function consultarHoras(){  // se carga la vista cuando se llama a la funcion
    $("#contenedor-listado-horas")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('tiempos/listado');?>");

  }
  consultarHoras();
</script>
<script >

    function eliminar(id_hor_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("tiempos/borrar"); ?>',
              data:{"id_hor_eda":id_hor_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalHoras").modal("hide");
                    consultarHoras();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }
              }
            });
    }

</script>

<script type="text/javascript">
  $("#frm-nueva-hora").validate({
    rules:{
      hora_hor_eda:{
        required:true
      },
      fecha_hor_eda:{
        required:true
      }
    },
    messages:{
      hora_hor_eda:{
        required:"Por favor ingrese la hora"
      },
      fecha_hor_eda:{
        required:"Por favor ingrese la fecha"
      }

  },

  submitHandler:function(formulario){
    var formData = new FormData($(formulario)[0]);
    $.ajax({
      type:'post',
      url:'<?php echo site_url("tiempos/guardar"); ?>',
      data:formData,
      contentType:false,
      processData:false,
      success:function(data){
        var objeto=JSON.parse(data);
        if (objeto.estado=="ok") {
          Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalHoras").modal("hide");
          consultarHoras();
        } else {
          Swal.fire('ERROR','Error al insertar. intente nuevamente','error');
        }
      }
    });
  }
  });
</script>

<script type="text/javascript">
  function cargarEdicion(id){
    $("#contenedor-edicion").load("<?php echo site_url('tiempos/editar'); ?>/"+id);
    $("#modalEditarHora").modal("show")
  }
  consultarHoras();
</script>
