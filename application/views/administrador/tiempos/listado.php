<?php if ($listadoTiempos): ?>
  <table id="tbl-tiempo" class="table table-bordered table-striped table-hover" style="color:#ffffff">
    <thead>
      <tr>
        <th class="text-center">ID: </th>
        <th class="text-center"><i class="fa fa-calendar" aria-hidden="true"></i> hora : </th>
        <th class="text-center"><i class="fa fa-calendar" aria-hidden="true"></i> fecha : </th>
        <th class="text-center">ACCIONES: </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoTiempos->result() as $tiempoTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $tiempoTemporal->id_hor_eda?></td>
          <td class="text-center"><?php echo $tiempoTemporal->hora_hor_eda?></td>
          <td class="text-center"><?php echo $tiempoTemporal->fecha_hor_eda?></td>

          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $tiempoTemporal->id_hor_eda?>);">
                  <i class="fa fa-pen"></i>

                </a>
                <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>
                <a onclick="eliminar(<?php echo $tiempoTemporal->id_hor_eda ?>);" class="btn btn-danger">
                              <i class="fa fa-trash"></i>
                            </a>
                            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="aler alert-danger">
    No se encontraron salas registrados.
  </div>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-tiempo").DataTable();
</script>
