<?php if ($tarifas): ?>
  <table id="tbl-tarifas" class="table table-bordered table-striped table-hover" style="color:#ffffff">
    <thead>
      <tr>
        <th class="text-center">ID: </th>
        <th class="text-center"><i class="	fas fa-male">TARIFA ADULTO: </th>
        <th class="text-center"><i class="	fas fa-male">TARIFA ESTUDIANTE: </th>
        <th class="text-center"><i class="	fas fa-male">TARIFA NIÑOS : </th>
        <th class="text-center"><i class="fab fa-accessible-icon">TARIFA PERSONAS DISCAPACITADAS : </th>
        <th class="text-center"><i class="	fas fa-male">TARIFA TERCERA EDAD : </th>
        <th class="text-center">ACCIONES: </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($tarifas->result() as $tarifasTemporal): ?>
        <tr>
          <td><?php echo $tarifasTemporal->id_tar_eda ?></td>
          <td><?php echo $tarifasTemporal->adulto_tar_eda ?></td>
          <td><?php echo $tarifasTemporal->estudiante_tar_eda?></td>
          <td><?php echo $tarifasTemporal->ninos_tar_eda?></td>
          <td><?php echo $tarifasTemporal->discapacitados_tar_eda?></td>
          <td><?php echo $tarifasTemporal->tercera_tar_eda?></td>
          <td>
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $tarifasTemporal->id_tar_eda ?>);">
                  <i class="fa fa-pen"></i>

                </a>
  <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>
            <a onclick="eliminar(<?php echo $tarifasTemporal->id_tar_eda ?>);" class="btn btn-danger">
              <i class="fa fa-trash"></i>
            </a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="aler alert-danger">
    No se encontraron tarifas registrados.
  </div>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-tarifas").DataTable();
</script>
