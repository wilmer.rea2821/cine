<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<form  id="frm_editar_tar" enctype="multipart/form-data" class="" action="<?php echo site_url("tarifas/actualizar"); ?>" method="post">
  <input type="hidden" name="id_tar_eda" value="<?php echo $tarifaEditar->id_tar_eda; ?>">
  <i class="	fas fa-male"></i><b>ADULTOS</b> <br>
  <select class="form-control text-center selectpicker" name="adulto_tar_eda" id="adulto_tar_ade" required data-live-search="true">
    <option value="">-------seleccione una tarifa para adulto---------</option>
    <option value="$5.50">$5.50</option>
    <option value="$6.00">$6.00</option>
    <option value="$6.50">$6.50</option>
  </select>
  <br>
  <i class="	fas fa-male"></i><b>ESTUDIANTES</b> <br>
  <select class="form-control text-center selectpicker" name="estudiante_tar_eda" id="estudiante_tar_ade" required data-live-search="true">
    <option value="">-------seleccione una tarifa para estudiantes---------</option>
    <option value="$1.50">$1.90</option>
    <option value="$2.00">$2.00</option>
    <option value="$2.50">$2.50</option>
  </select>
  <br>
  <i class="	fas fa-male"></i><b>NILOS</b> <br>
  <select class="form-control text-center selectpicker" name="ninos_tar_eda" id="ninos_tar_ade" required data-live-search="true">
    <option value="">-------seleccione una tarifa para niños---------</option>
    <option value="$1.50">$1.00</option>
    <option value="$2.00">$1.20</option>
    <option value="$2.50">$1.50</option>
  </select>
  <br>
  <i class="fab fa-accessible-icon"></i><b>DISCAPACITADOS</b> <br>
  <select class="form-control text-center selectpicker" name="discapacitados_tar_eda" id="discapacitados_tar_ade" required data-live-search="true">
    <option value="">-------seleccione una tarifa para personas con discapacidad---------</option>
    <option value="$1.50">$1.00</option>
    <option value="$2.00">$1.20</option>
    <option value="$2.50">$1.50</option>
  </select>
  <br>
  <i class="	fas fa-male"></i><b>TERCERA EDAD</b> <br>
  <select class="form-control text-center selectpicker" name="tercera_tar_eda" id="tercera_tar_ade" required data-live-search="true">
    <option value="">-------seleccione una tarifa para adulto-tercera edad---------</option>
    <option value="$1.50">$1.50</option>
    <option value="$2.00">$2.00</option>
    <option value="$2.50">$2.50</option>
  </select>
  <button type="button" onclick="actualizar();" name="button"
      class="btn btn-success">
        <i class="fa fa-pen"></i> Actualizar
      </button>
    </form>


    <script type="text/javascript">
    function actualizar(){

      $.ajax({
        url:$("#frm_editar_tar").prop("action"),
        data:$("#frm_editar_tar").serialize(),
        type:"post",
        success:function(data){
          consultartarifas();
          $("#modalEditarTarifa").modal("hide");
          $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
          $('.modal-backdrop').remove();//eliminamos el backdrop del modal
          var objeto=JSON.parse(data);
          if(objeto.estado=="ok" || objeto.respuesta=="OK"){
            Swal.fire('CONFIRMACION',objeto.mensaje,'success');
              $("#modalEditarTarifa").modal("hide");
              consultartarifas();
              redirect('tarifas/index');
          }else{
    Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
          }

        }
      });
    }
    </script>
<script type="text/javascript">
$("#adulto_tar_ade").val("<?php echo $tarifaEditar->adulto_tar_eda?>");
$("#estudiante_tar_ade").val("<?php echo $tarifaEditar->estudiante_tar_eda?>");
$("#ninos_tar_ade").val("<?php echo $tarifaEditar->ninos_tar_eda?>");
$("#discapacitados_tar_ade").val("<?php echo $tarifaEditar->discapacitados_tar_eda?>");
$("#tercera_tar_ade").val("<?php echo $tarifaEditar->tercera_tar_eda?>");
</script>
