<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalNuevaTarifa">
   <i class="glyphicon glyphicon-home"></i>REGISTRAR TARIFAS
</button></center>
<br><hr>
<legend class="text-center">
<i class="glyphicon glyphicon-plus"></i>
<b>GESTIÓN DE TARIFAS</b>
</legend>
<hr>

<!-- The Modal -->
<div class="modal" id="modalNuevaTarifa">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" style="color:#000000">REGISTRO DE TARIFAS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form  id="frm_nueva_tarifa" enctype="multipart/form-data" class="" action="<?php echo site_url("tarifas/guardar"); ?>" method="post">
          <i class="	fas fa-male"></i><b>ADULTOS</b> <br>
          <select class="form-control text-center" name="adulto_tar_eda" id="adulto_tar_eda" required >
            <option value="">-------seleccione una tarifa para adulto---------</option>
            <option value="$5.50">$5.50</option>
            <option value="$6.00">$6.00</option>
            <option value="$6.50">$6.50</option>
          </select>
          <br>
          <i class="	fas fa-male"></i><b>ESTUDIANTES</b> <br>
          <select class="form-control text-center" name="estudiante_tar_eda" id="estudiante_tar_eda" required >
            <option value="">-------seleccione una tarifa para estudiantes---------</option>
            <option value="$1.50">$1.90</option>
            <option value="$2.00">$2.00</option>
            <option value="$2.50">$2.50</option>
          </select>
          <br>
          <i class="	fas fa-male"></i><b>NILOS</b> <br>
          <select class="form-control text-center" name="ninos_tar_eda" id="ninos_tar_eda" required >
            <option value="">-------seleccione una tarifa para niños---------</option>
            <option value="$1.50">$1.00</option>
            <option value="$2.00">$1.20</option>
            <option value="$2.50">$1.50</option>
          </select>
          <br>
          <i class="fab fa-accessible-icon"></i><b>DISCAPACITADOS</b> <br>
          <select class="form-control text-center" name="discapacitados_tar_eda" id="discapacitados_tar_eda" required >
            <option value="">-------seleccione una tarifa para personas con discapacidad---------</option>
            <option value="$1.50">$1.00</option>
            <option value="$2.00">$1.20</option>
            <option value="$2.50">$1.50</option>
          </select>
          <br>
          <i class="	fas fa-male"></i><b>TERCERA EDAD</b> <br>
          <select class="form-control text-center" name="tercera_tar_eda" id="tercera_tar_eda" required >
            <option value="">-------seleccione una tarifa para adulto-tercera edad---------</option>
            <option value="$1.50">$1.50</option>
            <option value="$2.00">$2.00</option>
            <option value="$2.50">$2.50</option>
          </select>
          <br>
          <button type="submit"class="btn btn-success" name="button">guardar</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>
<div class="container">
  <div id="contenedor-listado-tarifa">
    <!-- fa-lg(tamaño)/fa-6x(tamaño)/ fa-spin(efecto de rotar)-->
  </div>
</div>



<div class="modal" id="modalEditarTarifa">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

        <h4 class="modal-title" style="color:#000000">ACTUALIZACIÓN DE TARIFAS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion-tarifa">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>



<!-- inicio script funcion consultarProfesores id(#) clase(.) -->
<script type="text/javascript">
  function consultartarifas(){  // se carga la vista cuando se llama a la funcion
    $("#contenedor-listado-tarifa")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('tarifas/listado');?>");

  }
  consultartarifas();
</script>
<script >

    function eliminar(id_tar_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("tarifas/borrar"); ?>',
              data:{"id_tar_eda":id_tar_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalNuevaTarifa").modal("hide");
                    consultartarifas();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }


              }
            });


    }

</script>
<script type="text/javascript">
   $("#frm_nueva_tarifa").validate({
      rules:{
          adulto_tar_eda:{
            required:true
          },
          estudiante_tar_eda:{
            required: true
          },
          ninos_tar_eda:{
            required: true
          },
          discapacitados_tar_eda:{
            required: true
        },
        tercera_tar_eda:{
          required: true
        }
      },
      messages:{
        adulto_tar_eda:{
          required:"Por favor ingrese la tarifa para adultos"
        },
        estudiante_tar_eda:{
          required: "Por favor ingrese la tarifa para estudiantes"
        },
        ninos_tar_eda:{
          required: "Por favor ingrese la tarifa para niños"
        },
        discapacitados_tar_eda:{
          required:"Por favor ingrese la tarifa para personas con discapacidad"
        },
        tercera_tar_eda:{
        required:"Por favor ingrese la tarifa para personas de la tercera edad"
      }

    },
      submitHandler:function(formulario){

        var formData = new FormData($(formulario)[0]);
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url("tarifas/guardar"); ?>',
          data:formData,
          contentType: false,
          processData: false,
          success:function(data){
            var objeto=JSON.parse(data);
            if (objeto.estado=="ok") {
              Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                $("#modalNuevaTarifa").modal("hide");
                consultartarifas();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        });
      }
   });
</script>


<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('tarifas/editar'); ?>/"+id);
      $("#contenedor-edicion-tarifa").load("<?php echo site_url('tarifas/editar'); ?>/"+id);
      $("#modalEditarTarifa").modal("show");
    }
    consultartarifas();
</script>
