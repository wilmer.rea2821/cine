<?php if ($listadofunciones): ?>
  <table id="tabla-funciones" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-center">ID</td>
        <td class="text-center">NOMBRE</td>
        <td class="text-center">PELICULA</td>
        <td class="text-center">SALA</td>
        <td class="text-center">HORA</td>
        <td class="text-center">ACCIONES</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadofunciones->result() as $funcionTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $funcionTemporal->id_fun_eda ?></td>
          <td class="text-center"><?php echo $funcionTemporal->nombre_funcion ?></td>
          <td class="text-center"><?php echo $funcionTemporal->nombre_pel_eda ?></td>
          <td class="text-center"><?php echo $funcionTemporal->nombre_sal_eda ?></td>
          <td class="text-center"><?php echo $funcionTemporal->hora_hor_eda ?></td>
          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $funcionTemporal->id_fun_eda ?>);">
                  <i class="glyphicon glyphicon-edit"></i>
                  Editar
                </a>
               <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>
                <a onclick="eliminar(<?php echo $funcionTemporal->id_fun_eda ?>);" class="btn btn-danger">
                  <i class="fa fa-trash"></i>

                </a>
                <?php endif; ?>
          </td>
        </tr>
        <script type="text/javascript">
        $("#tabla-funciones").dataTable();

        </script>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
<h3>
  <b>No existe Funciones</b>
</h3>
<?php endif; ?>
