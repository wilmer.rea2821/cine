<br>
<br>
<legend class="text-center">
  <h1> <b>GESTION DE FUNCIONES</b> </h1>
</legend>
<br>
<!-- Button to Open the Modal -->
<center><button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#modalNuevaFuncion" class="text-center">
  Agregar nuevo
</button></center>

<!-- The Modal -->
<div class="modal" id="modalNuevaFuncion">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title " style="color:#000000">INGRESAR NUEVA FUNCION</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm_nueva_funcion"class="" enctype="multipart/form-data" action="<?php echo site_url('funciones/guardarFunciones') ?>" method="post">
          <b>Nombre</b> <br>
          <select class="form-control" name="nombre_funcion" id="nombre_funcion" required >
            <option value="">-------seleccione una funcion---------</option>
            <option value="Matutina">Matutina</option>
            <option value="vespertina">vespertina</option>
            <option value="Nocturna">Nocturna</option>
          </select>
          <br>
          <b>Pelicula</b> <br>
          <div class="col-md-7">
  <select class="form-control" name="fk_id_pel_eda" id="fk_id_pel_eda" required data-live-search="true">
    <!-- cargar opciones de <select class="" name="">  </select> -->
    <option value="">-------------selecciones el pelicula-----------</option>
    <?php if ($listadoPeliculas): ?>
      <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
        <option value="<?php echo $peliculaTemporal->id_pel_eda;?>">
        <?php echo $peliculaTemporal->nombre_pel_eda; ?>
        -
        <?php echo $peliculaTemporal->tiempo_pel_eda; ?>
        </option>
      <?php endforeach; ?>

    <?php endif; ?>

</select>
</div>
          <br>
          <b>Sala</b> <br>
          <div class="col-md-7">
  <select class="form-control" name="fk_id_sal_eda" id="fk_id_sal_eda" required data-live-search="true">
    <!-- cargar opciones de <select class="" name="">  </select> -->
    <option value="">-------------selecciones el sala -----------</option>
    <?php if ($listadoSalas): ?>
      <?php foreach ($listadoSalas->result() as $salaTemporal): ?>
        <option value="<?php echo $salaTemporal->id_sal_eda;?>">
        <?php echo $salaTemporal->nombre_sal_eda; ?>

        </option>

      <?php endforeach; ?>

    <?php endif; ?>

</select>
</div>
          <br>
          <b>Hora</b> <br>
          <div class="col-md-7">

  <select class="form-control"   name="fk_id_hor_eda" id="fk_id_hor_eda" required data-live-search="true">
    <!-- cargar opciones de <select class="" name="">  </select> -->
    <option value="">-------------selecciones el Hora-----------</option>
    <?php if ($listadoHoras): ?>
      <?php foreach ($listadoHoras->result() as $horaTemporal): ?>
        <option value="<?php echo $horaTemporal->id_hor_eda;?>">
        <?php echo $horaTemporal->hora_hor_eda; ?>

        </option>

      <?php endforeach; ?>

    <?php endif; ?>

</select>
</div>
          <br>
          <button type="submit" name="button" class="btn btn-success"> <i class="glyphicon glyphicon-ok"></i> Guardar</button>

        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-bs-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>


<div class="container">
  <div class="" id="listado-funciones">

  </div>

</div>
<!-- inicio modal -->
<div class="modal" id="modalEditarFunciones">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

        <h4 class="modal-title" style="color:#000000">ACTUALIZACIÓN DE FUNCIONES</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion-funciones">

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- fin modal -->



<br>

</script>
<script type="text/javascript">
function consultarFunciones(){
  $("#listado-funciones").html('<center><i class="fa fa-spinner fa-spin fa-3x "> </i><br> Espere por favor...</center>').load("<?php echo site_url('funciones/listado') ?>");


}
consultarFunciones(); //llamar a la funcion para que actualice la tabla automaticamente
</script>


<!-- validaciones-->
<script >

    function eliminar(id_fun_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("funciones/borrar"); ?>',
              data:{"id_fun_eda":id_fun_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalNuevaConfiguraciona").modal("hide");
                    consultarFunciones();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }


              }
            });


    }

</script>
<script type="text/javascript">
   $("#frm_nueva_funcion").validate({
     rules:{
       nombre_funcion:{
         required:true
       },
       fk_id_pel_eda:{
         required:true
       },
       fk_id_sal:{
         required:true
       },
       fk_id_hor:{
         required:true

       }


     },
     messages:{
       nombre_funcion:{
         required:"Selecione una jornada "
       },
       fk_id_pel_eda:{
         required:"selecione una pelicula"
       },
       fk_id_sal:{
         required:"seleccione una sala "
       },
       fk_id_hor:{
         required:"seleccione una hora"
       }

     },


      submitHandler:function(formulario){
        // ejecutando la peticion Asincrona
        $.ajax({
          type:'post',
          url:'<?php echo site_url('funciones/guardarFunciones') ?>',
          data:$(formulario).serialize(),
          success:function(data){
            var objetoRespuesta=JSON.parse(data);
            if (objetoRespuesta.estado=="ok") {
              Swal.fire('confirmacion',objetoRespuesta.mensaje,'success');
                $("#modalNuevaFuncion").modal("hide");
                consultarFunciones();
            }else {
              Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

            }


          }
        }); 
      }
   });
</script>

<script type="text/javascript">
    function cargarEdicion(id){
     // alert("<?php echo site_url('configuraciones/editar'); ?>/"+id);
      $("#contenedor-edicion-funciones").load("<?php echo site_url('funciones/editar'); ?>/"+id);
      $("#modalEditarFunciones").modal("show");
      consultarFunciones();
    }
</script>
