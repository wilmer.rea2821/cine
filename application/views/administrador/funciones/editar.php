<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js">

</script> -->
<div class="" style="background-color:black;">

</div>
<div class="container">
  <?php if ($funcionEditar): ?>
    <form id="frm_editar_funciones" enctype="multipart/form-data" class="" action=" <?php echo site_url('funciones/procesoActualizar') ?>" method="post">
      <input type="hidden" name="id_fun_eda" value="<?php echo $funcionEditar->id_fun_eda ?>">
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Jornada: </label>
        </div>
        <div class="col-md-7">
          <select class="form-control selectpicker" name="nombre_funcion" id="fk_id_est" required data-live-search="true">
            <option value="">-------seleccione una funcion---------</option>
            <option value="Matutina">Matutina</option>
            <option value="vespertina">vespertina</option>
            <option value="Nocturna">Nocturna</option>
          </select>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Peliculas: </label>
    </select>
        </div>
        <div class="col-md-7">
          <select class="form-control selectpicker" name="fk_id_pel_eda" id="fk_id_asi" required data-live-search="true">
            <option value="">-------------selecciones el pelicula-----------</option>
            <?php if ($listadoPeliculas): ?>
              <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
                <option value="<?php echo $peliculaTemporal->id_pel_eda;?>">
                <?php echo $peliculaTemporal->nombre_pel_eda; ?>
                -
                <?php echo $peliculaTemporal->tiempo_pel_eda; ?>
                </option>
              <?php endforeach; ?>

            <?php endif; ?>
          </select>
        </div>

      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Sala: </label>
    </select>
        </div>
        <div class="col-md-7">
          <select class="form-control selectpicker" name="fk_id_sal_eda" id="fk_id_tipo" required data-live-search="true">
            <option value="">-------------selecciones el sala -----------</option>
            <?php if ($listadoSalas): ?>
              <?php foreach ($listadoSalas->result() as $salaTemporal): ?>
                <option value="<?php echo $salaTemporal->id_sal_eda;?>">
                <?php echo $salaTemporal->nombre_sal_eda; ?>

                </option>

              <?php endforeach; ?>

            <?php endif; ?>
          </select>
        </div>

      </div>
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Hora: </label>
        </div>
        <div class="col-md-7">
          <select class="form-control"   name="fk_id_hor_eda" id="id_hor_eda" required data-live-search="true">
            <!-- cargar opciones de <select class="" name="">  </select> -->
            <option value="">-------------selecciones el Hora-----------</option>
            <?php if ($listadoHoras): ?>
              <?php foreach ($listadoHoras->result() as $horaTemporal): ?>
                <option value="<?php echo $horaTemporal->id_hor_eda;?>">
                <?php echo $horaTemporal->hora_hor_eda; ?>

                </option>

              <?php endforeach; ?>

            <?php endif; ?>

        </select>        </div>
      </div>
      <br>
       <div class="row text-center">
      <div class="col-md-7">
        <button type="submit" name="button" onclick="actualizar();" class="btn btn-success" onclick="actualizar();">
          <i class="glyphicon glyphicon-refresh"></i>
          ACTUALIZAR
        </button>
      </div>
    </div>
    </form>

  </div>
  <?php else: ?>
    <div class="alert alert-danger">
      <b>No se encontra funcines </b>

    </div>
  <?php endif; ?>
<!-- validacion -->




<script type="text/javascript">
function actualizar(){

  $.ajax({
    url:$("#frm_editar_funciones").prop("action"),
    data:$("#frm_editar_funciones").serialize(),
    type:"post",
    success:function(data){
      consultarFunciones();
      $("#modalEditarFunciones").modal("hide");
      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('.modal-backdrop').remove();//eliminamos el backdrop del modal
      var objeto=JSON.parse(data);
      if(objeto.estado=="ok" || objeto.respuesta=="OK"){
        Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalEditarFunciones").modal("hide");
          consultarFunciones();
          redirect('funciones/index');
      }else{
Swal.fire('ERROR','Error al insertar, intente nuevamente','error');
      }

    }
  });
}
</script>

<script type="text/javascript">
$("#fk_id_est").val("<?php echo $funcionEditar->nombre_funcion ?>");
$("#fk_id_asi").val("<?php echo $funcionEditar->fk_id_pel_eda?>");
$("#fk_id_tipo").val("<?php echo $funcionEditar->fk_id_sal_eda?>");
$("#id_hor_eda").val("<?php echo $funcionEditar->fk_id_hor_eda ?>");

</script>
