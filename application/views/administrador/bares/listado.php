<?php if ($listadobar): ?>
  <table id="tbl-bar" class="table table-bordered table-striped table-hover" style="color:#ffffff">
    <thead>
      <tr>
        <th class="text-center">ID: </th>
        <th class="text-center"><i class="fas fa-shopping-cart"></i> NOMBRE DE PRODUCTO : </th>
        <th class="text-center"><i class="fas fa-shopping-cart"></i> PRECIO DE PRODUCTO: </th>
        <th class="text-center"><i class="fas fa-shopping-cart"></i> CANTIDAD DE PRODUCTO: </th>
        <th class="text-center">ACCIONES: </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadobar->result() as $barTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $barTemporal->id_bar_eda?></td>
          <td class="text-center"><?php echo $barTemporal->nombre_bar_eda?></td>
          <td class="text-center"><?php echo $barTemporal->precio_bar_eda?></td>
          <td class="text-center"><?php echo $barTemporal->cantidad_bar_eda?></td>
          <td class="text-center">
            <a href="javascript:void(0)" class="btn btn-warning" onclick="cargarEdicion(<?php echo $barTemporal->id_bar_eda ?>);">
                  <i class="fa fa-pen"></i>

                </a>
                <?php if ($this->session->userdata('conectad0')->perfil_usu_eda=="ADMINISTRADOR"): ?>
                <a onclick="eliminar(<?php echo $barTemporal->id_bar_eda ?>);" class="btn btn-danger">
                              <i class="fa fa-trash"></i>
                            </a>
                            <?php endif; ?>

          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="aler alert-danger">
    No se encontraron asientos registrados.
  </div>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-bar").DataTable();
</script>
