<!-- Button to Open the Modal -->
<br>
<center><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalBares">
   <i class="fa fa-cart-plus" aria-hidden="true"></i> REGISTRAR PRODUCTO
</button></center>
<br><hr>
<legend class="text-center">
<i class="fas fa-shopping-cart"></i>
<b>GESTIÓN DE BAR</b>
</legend>
<hr>

 
<!-- The Modal -->
<div class="modal" id="modalBares">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"style="color:#000000">REGISTRO DE PRODUCTOS</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form id="frm-nuevo-producto" class="" enctype="multipart/form-data" action="<?php echo site_url('bares/guardar'); ?>" method="post">
          <i class="fas fa-shopping-cart"></i>  <b>NOMBRE PRODUCTO</b>
          <br>
          <input type="text" id="nombre_bar_eda" name="nombre_bar_eda" value="" placeholder="Por favor Ingrese nombre del producto" class="form-control"><br>
          <i class="fas fa-shopping-cart"></i> <b>PRECIO DE PRODUCTO</b>
          <br>
          <input type="number" id="precio_bar_eda" name="precio_bar_eda" value="" placeholder="Por favor ingrese precio del producto" class="form-control"><br>
          <i class="fas fa-shopping-cart"></i> <b>CANTIDAD PRODUCTO</b>
          <br>
          <input type="number" id="cantidad_bar_eda" name="cantidad_bar_eda" value="" placeholder="Por favor ingrese cantidad de producto" class="form-control"><br>
            <button type="submit" name="button" class="btn btn-success"> <i class="fa fa-check-square" aria-hidden="true"></i> Guardar</button>
        </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>
<!-- inicio contenedor -->
<div class="container">
  <div id="contenedor-listado-producto">

  </div>
</div>

<!-- inicio modal editar -->
<!-- The Modal -->
<div class="modal" id="modalEditarBar">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"style="color:#000000">ACTUALIZAR PRODUCTO</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="contenedor-edicion">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>

    </div>
  </div>
</div>


<!-- inicio script consultar -->
<script type="text/javascript">
  function consultarBar(){
    $("#contenedor-listado-producto")
    .html('<center><i class="fa fa-spinner fa-spin fa-2x"></i> <br>Espere Por Favor</center>')
    .load("<?php echo site_url('bares/listado'); ?>")
  }
  consultarBar();
</script>
<script >

    function eliminar(id_bar_eda){

            //!Ejecutando la peticion asincrona
            $.ajax({
              type:'post',
              url:'<?php echo site_url("bares/borrar"); ?>',
              data:{"id_bar_eda":id_bar_eda},
              success:function(data){
                var objeto=JSON.parse(data);
                if (objeto.estado=="ok") {
                  Swal.fire('CONFIRMACION',objeto.mensaje,'success');
                    $("#modalBares").modal("hide");
                    consultarBar();
                }else {
                  Swal.fire('ERROR','Error al insertar, intente nuevamente','error');

                }
              }
            });
    }

</script>

<script type="text/javascript">
  $("#frm-nuevo-producto").validate({
    rules:{
        nombre_bar_eda:{
          required:true
        },
        precio_bar_eda:{
          required: true,
          // minlength:4,
          // maxlength:4,
          // digits:true
        },
        cantidad_bar_eda:{
          required: true,
          // minlength:10,
          // maxlength:10,
          //  digits:true

    }
    },
    messages:{
      nombre_bar_eda:{
        required:"Por favor ingrese el nombre del producto"
      },
      precio_bar_eda:{
        required: "Por favor ingrrese el precio del producto",

      },
      cantidad_bar_eda:{
        required: "Por favor ingrese la cantidad del producto",

      },
      direccion_con_eda:{
        required:"Por favor ingrese la direccion"
      },

    representante_con_eda:{
      required:"Por favor ingrese el representante de la empresa"
    },
    foto_con_eda:{
      required:"Por favor seleciona una foto"
    }

  },

  submitHandler:function(formulario){
    var formData = new FormData($(formulario)[0]);
    $.ajax({
      type:'post',
      url:'<?php echo site_url("bares/guardar"); ?>',
      data:formData,
      contentType:false,
      processData:false,
      success:function(data){
        var objeto=JSON.parse(data);
        if (objeto.estado=="ok") {
          Swal.fire('CONFIRMACION',objeto.mensaje,'success');
          $("#modalBares").modal("hide");
          consultarBar();
        } else {
          Swal.fire('ERROR','Error al insertar. intente nuevamente','error');
        }
      }
    });
  }
  });
</script>

<script type="text/javascript">
  function cargarEdicion(id){
    $("#contenedor-edicion").load("<?php echo site_url('bares/editar'); ?>/"+id);
    $("#modalEditarBar").modal("show")
  }
  consultarBar();
</script>
