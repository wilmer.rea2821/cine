<?php
   class Sucursal extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("sucursal_eda",$datos);
     }
     //Funcion que consulta todos los profesores de la bdd
     public function obtenerTodos(){
        $sucursales=$this->db->get("sucursal_eda");
        if ($sucursales->num_rows()>0) {
          return $sucursales;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un profesor se recibe el id
     public function eliminarPorId($id_suc_eda){
        $this->db->where("id_suc_eda",$id_suc_eda);
        return $this->db->delete("sucursal_eda");
     }
     //Consultando el profesor por su id
     public function obtenerPorId($id){
        $this->db->where("id_suc_eda",$id);
        $sucursales=$this->db->get("sucursal_eda");
        if($sucursales->num_rows()>0){
          return $sucursales->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de profesores
     public function actualizar($id,$datos){
       $this->db->where("id_suc_eda",$id);
       return $this->db->update("sucursal_eda",$datos);
     }





   }//Cierre de la clase (No borrar)
