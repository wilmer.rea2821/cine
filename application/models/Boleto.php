<?php
/**
 *
 */
class Boleto extends CI_Model
{

  function __construct()
  {
    parent::__construct();

  }

  public function insertar($datos){
    return $this->db->insert("ventaboletos",$datos);

  }
  ///funcion que consulta todos los estudiantes de la bdd


  public function obtenerTodo(){
    $boletos=$this->db->get('ventaboletos');  ///cambiar las bariables $estudiantes y el nombre de las base de daros
    ///validamos la base de datos
     if ($boletos->num_rows()>0) {
      return $boletos;

     }else {
       return  false;

     }
  }


//eliminar funciones
public function elimanarPorId($id){
  $this->db->where("id_ven_eda",$id);
  return $this->db->delete("ventaboletos");
}


  //actualisar los datos en la base de datos
  //consultando al estudiante por su //
  public function obtenerPorId($id){
    $this->db->where("id_ven_eda",$id);
    $boleto=$this->db->get(" ventaboletos");
    //validacion si existe un estudiantes
    if ($boleto->num_rows()>0) {
      return $boleto->row();//si existe
    } else {
      return false;
    }
  }

  // join de la tabla ventaboletos
  public function obtenerTodas(){
  $this->db->join("asientos_eda",
  "asientos_eda.id_asi_eda=ventaboletos.fk_id_asi_eda");
  $this->db->join("peliculas",
  "peliculas.id_pel_eda=ventaboletos.fk_id_pel_eda");
  $this->db->join("tarifas_eda",
  "tarifas_eda.id_tar_eda=ventaboletos.fk_id_tar_eda");
  $ventas=$this->db->get("ventaboletos");
  if($ventas->num_rows()>0){
    return $ventas;
  }
  return false;
}


public function actualizar($id,$datos){
  $this->db->where("id_ven_eda",$id);
  return $this->db->update("ventaboletos",$datos);
}

}//cierre
