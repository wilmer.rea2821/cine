<?php
class Pelicula extends CI_Model
{

  function __construct()
  {
    parent:: __construct();
  }


  public function insertar($datos){
     return $this->db->insert("peliculas",$datos);
  }
  public function obtenerTodos(){
    $peliculas=$this->db->get("peliculas");
    if ($peliculas->num_rows()>0) {
      return $peliculas;
    } else {
      return false;
    }

  }

  //funcion para eliminar a un estudiante mediante su id
      public function eliminarPorId($id){
          $this->db->where("id_pel_eda", $id);
          return $this->db->delete("peliculas");
      }

  public function obtenerPorId($id){
    $this->db->where("id_pel_eda",$id);
    $peliculas=$this->db->get("peliculas");
    if ($peliculas->num_rows()>0) {
      return $peliculas->row();
    } else {
      return false;
    }
  }

  public function actualizar($id,$datos){
    $this->db->where("id_pel_eda",$id);
    return $this->db->update("peliculas",$datos);
  }
}
