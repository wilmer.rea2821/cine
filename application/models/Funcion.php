<?php
/**
 *
 */
class Funcion extends CI_Model
{

  function __construct()
  {
    parent::__construct();

  }

  public function insertar($datos){
    return $this->db->insert("funciones",$datos);

  }
  ///funcion que consulta todos los estudiantes de la bdd


  public function obtenerTodo(){
    $funcion=$this->db->get('funciones');  ///cambiar las bariables $estudiantes y el nombre de las base de daros
    ///validamos la base de datos
     if ($funcion->num_rows()>0) {
      return $funcion;

     }else {
       return  false;

     }
  }


//eliminar funciones
public function elimanarPorId($id){
  $this->db->where("id_fun_eda",$id);
  return $this->db->delete("funciones");
}


  //actualisar los datos en la base de datos
  //consultando al estudiante por su //
  public function obtenerPorId($id){
    $this->db->where("id_fun_eda",$id);
    $funcione=$this->db->get("funciones");
    //validacion si existe un estudiantes
    if ($funcione->num_rows()>0) {
      return $funcione->row();//si existe
    } else {
      return false;
    }
  }

  // join de la tabla funcionesa
  public function obtenerTodas(){
  $this->db->join("peliculas",
  "peliculas.id_pel_eda=funciones.fk_id_pel_eda");
  $this->db->join("sala",
  "sala.id_sal_eda=funciones.fk_id_sal_eda");
  $this->db->join("hora",
  "hora.id_hor_eda=funciones.fk_id_hor_eda");
  $funciones=$this->db->get("funciones");
  if($funciones->num_rows()>0){
    return $funciones;
  }
  return false;
}


public function actualizar($id,$datos){
  $this->db->where("id_fun_eda",$id);
  return $this->db->update("funciones",$datos);
}

}//cierre
