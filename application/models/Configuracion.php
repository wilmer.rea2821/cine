<?php
class Configuracion extends CI_Model
{

  function __construct()
  {
    parent:: __construct();
  }


  public function insertar($datos){
     return $this->db->insert("configuracion_eda",$datos);
  }
  public function obtenerTodos(){
    $configuraciones=$this->db->get("configuracion_eda");
    if ($configuraciones->num_rows()>0) {
      return $configuraciones;
    } else {
      return false;
    }
 
  }

  //funcion para eliminar a un estudiante mediante su id
      public function eliminarPorId($id){
          $this->db->where("id_con_eda", $id);
          return $this->db->delete("configuracion_eda");
      }

  public function obtenerPorId($id){
    $this->db->where("id_con_eda",$id);
    $configuraciones=$this->db->get("configuracion_eda");
    if ($configuraciones->num_rows()>0) {
      return $configuraciones->row();
    } else {
      return false;
    }
  }

  public function actualizar($id,$datos){
    $this->db->where("id_con_eda",$id);
    return $this->db->update("configuracion_eda",$datos);
  }
}
