
<?php
   class Tiempo extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("hora",$datos);
     }
     //Funcion que consulta todos los profesores de la bdd
     public function obtenerTodo(){
         $tiempos=$this->db->get("hora");
         // validar si tiene datos o no
         if ($tiempos->num_rows()>0) {
           return $tiempos;
         } else {
           return false;
         }

       }
     //funcion para eliminar un profesor se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_hor_eda",$id);
        return $this->db->delete("hora");
     }
     //Consultando el profesor por su id
     public function obtenerPorId($id){
        $this->db->where("id_hor_eda",$id);
        $tiempos=$this->db->get("hora");
        if($tiempos->num_rows()>0){
          return $tiempos->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de profesores
     public function actualizar($id,$datos){
       $this->db->where("id_hor_eda",$id);
       return $this->db->update("hora",$datos);
     }





   }//Cierre de la clase (No borrar)
