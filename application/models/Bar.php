<?php
   class Bar extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("bar",$datos);
     }
     //Funcion que consulta todos los profesores de la bdd
     public function obtenerTodos(){
        $bares=$this->db->get("bar");
        if ($bares->num_rows()>0) {
          return $bares;
        } else {
          return false;//cuando no hay datos
        }
     } 
     //funcion para eliminar un profesor se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_bar_eda",$id);
        return $this->db->delete("bar");
     }
     //Consultando el profesor por su id
     public function obtenerPorId($id){
        $this->db->where("id_bar_eda",$id);
        $bares=$this->db->get("bar");
        if($bares->num_rows()>0){
          return $bares->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de profesores
     public function actualizar($id,$datos){
       $this->db->where("id_bar_eda",$id);
       return $this->db->update("bar",$datos);
     }





   }//Cierre de la clase (No borrar)
