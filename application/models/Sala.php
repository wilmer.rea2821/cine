<?php
   class Sala extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("sala",$datos);
     }
     //Funcion que consulta todos los profesores de la bdd
     public function obtenerTodos(){
        $salas=$this->db->get("sala");
        if ($salas->num_rows()>0) {
          return $salas;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un profesor se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_sal_eda",$id);
        return $this->db->delete("sala");
     }
     //Consultando el profesor por su id
     public function obtenerPorId($id){
        $this->db->where("id_sal_eda",$id);
        $salas=$this->db->get("sala");
        if($salas->num_rows()>0){
          return $salas->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de profesores
     public function actualizar($id,$datos){
       $this->db->where("id_sal_eda",$id);
       return $this->db->update("sala",$datos);
     }





   }//Cierre de la clase (No borrar)
