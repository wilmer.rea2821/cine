<?php

class Usuario extends CI_Model
{

  function __construct()
  {
    parent:: __construct();
  }
  public function obtenerPorEmailPassword($email,$password){
    $this->db->where("email_usu_eda",$email);
    $this->db->where("password_usu_eda",$password);
    $usuario=$this->db->get("usuarios");
    if ($usuario->num_rows()>0) {
      return $usuario->row();
    } else {
      return false;
    }

  }

  public function insertar($datos){
     return $this->db->insert("usuarios",$datos);
  }
  public function obtenerTodos(){
    $usuarios=$this->db->get("usuarios");
    if ($usuarios->num_rows()>0) {
      return $usuarios;
    } else {
      return false;
    }

  }

  public function eliminarPorId($id_usu_eda){
    $this->db->where("id_usu_eda",$id_usu_eda);
    return $this->db->delete("usuarios");
  }

  public function obtenerPorId($id){
    $this->db->where("id_usu_eda",$id);
    $usuario=$this->db->get("usuarios");
    if ($usuario->num_rows()>0) {
      return $usuario->row();
    } else {
      return false;
    }

  }

  public function actualizar($id,$datos){
    $this->db->where("id_usu_eda",$id);
    return $this->db->update("usuarios",$datos);
  }
}
