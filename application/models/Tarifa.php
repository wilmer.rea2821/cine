<?php
class Tarifa extends CI_Model
{

  function __construct()
  {
    parent:: __construct();
  }


  public function insertar($datos){
     return $this->db->insert("tarifas_eda",$datos);
  }
  public function obtenerTodos(){
    $tarifas=$this->db->get("tarifas_eda");
    if ($tarifas->num_rows()>0) {
      return $tarifas;
    } else {
      return false;
    }

  }

  //funcion para eliminar a un estudiante mediante su id
      public function eliminarPorId($id){
          $this->db->where("id_tar_eda", $id);
          return $this->db->delete("tarifas_eda");
      }

  public function obtenerPorId($id){
    $this->db->where("id_tar_eda",$id);
    $tarifas=$this->db->get("tarifas_eda");
    if ($tarifas->num_rows()>0) {
      return $tarifas->row();
    } else {
      return false;
    }
  }

  public function actualizar($id,$datos){
    $this->db->where("id_tar_eda",$id);
    return $this->db->update("tarifas_eda",$datos);
  }
}
