<?php
class Asiento extends CI_Model
{

  function __construct()
  {
    parent:: __construct();
  }


  public function insertar($datos){
     return $this->db->insert("asientos_eda",$datos);
  }
  public function obtenerTodos(){
    $asientos=$this->db->get("asientos_eda");
    if ($asientos->num_rows()>0) {
      return $asientos;
    } else {
      return false;
    }

  }

  //funcion para eliminar a un estudiante mediante su id
      public function eliminarPorId($id){
          $this->db->where("id_asi_eda", $id);
          return $this->db->delete("asientos_eda");
      }

  public function obtenerPorId($id){
    $this->db->where("id_asi_eda",$id);
    $asientos=$this->db->get("asientos_eda");
    if ($asientos->num_rows()>0) {
      return $asientos->row();
    } else {
      return false;
    }
  }

  public function actualizar($id,$datos){
    $this->db->where("id_asi_eda",$id);
    return $this->db->update("asientos_eda",$datos);
  }
}
