<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class seguridades extends CI_Controller {

	public function __construct(){
		parent:: __construct();
    $this->load->model("usuario");
		$this->load->model("autenticacion");
	}

  public function login(){

    $this->load->view('administrador/seguridades/login');
  }

 
  public function validarUsuario(){
    $email=$this->input->post("email_usu_eda");
    $password=$this->input->post("password_usu_eda");
    $usuarioEncontrado=$this->usuario->obtenerPorEmailPassword($email,$password);
    if ($usuarioEncontrado) {
      $this->session->set_userdata('conectad0',$usuarioEncontrado);
			$dataLogs=array(
			"fk_id_usu_eda"=>$usuarioEncontrado->id_usu_eda,
			"descripcion_eda"=>"INGRESAR"
			);
			$this->autenticacion->insertar($dataLogs);
      $this->session->set_flashdata("confirmacion","Bienvenido");
      redirect("welcome/administrador");
    } else {
      $this->session->set_flashdata("error","Email o congtraseña incorrectos");
      redirect("seguridades/login");
    }

  }

}//cierre de la clase
