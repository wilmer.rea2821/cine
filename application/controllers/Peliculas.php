<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peliculas extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("pelicula");
	}

	public function editar($id){
		$data["pelEditar"]=$this->pelicula->obtenerPorId($id);
		$this->load->view('administrador/peliculas/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
      "nombre_pel_eda"=>$this->input->post("nombre_pel_eda"),
			"tipo_pel_eda"=>$this->input->post("tipo_pel_eda"),
			"tiempo_pel_eda"=>$this->input->post("tiempo_pel_eda"),
			"detalle_pel_eda"=>$this->input->post("detalle_pel_eda"),
			"restriccion_pel_eda"=>$this->input->post("restriccion_pel_eda"),
		);
    $id_pel_eda=$this->input->post('id_pel_eda');

		if ($this->pelicula->actualizar($id_pel_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Pelicula editada exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}


  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/peliculas/index');
		$this->load->view('administrador/pie');
	}//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datospelicula=array(
      "nombre_pel_eda"=>$this->input->post("nombre_pel_eda"),
			"tipo_pel_eda"=>$this->input->post("tipo_pel_eda"),
			"tiempo_pel_eda"=>$this->input->post("tiempo_pel_eda"),
			"detalle_pel_eda"=>$this->input->post("detalle_pel_eda"),
			"restriccion_pel_eda"=>$this->input->post("restriccion_pel_eda"),
    );

				$this->load->library("upload"); //Activando la libreria de subidas de archivos
				$new_name = "foto_" . time() . "_" . rand(1, 5000); //Generando un nombre aleatorio
				$config['file_name'] = $new_name; //
				$config['upload_path'] = FCPATH . 'uploads/peliculas/';// configurando en cual carpeta se ba a subir el archivo
				$config['allowed_types'] = 'jpg|png|jpeg'; //dos extenciones especificas pdf|docx
				$config['max_size']  = 5*1024; //tamaño de la foto 2MB
				$this->upload->initialize($config);//inicializando la configuracion
				//Validando la subida de archivo
				if ($this->upload->do_upload("foto_pel_eda")) {
					//Que se subio con exito
					$dataSubida = $this->upload->data(); //Capturar el dato que se a subido
					$datospelicula["foto_pel_eda"] = $dataSubida['file_name'];
				}

    if ($this->pelicula->insertar($datospelicula)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Pelicula insertada exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		$data["lisPeliculas"]=$this->pelicula->obtenerTodos();
		$this->load->view("administrador/peliculas/listado",$data);
	}//fin funcion listado

	public function borrar(){
		$id_pel_eda=$this->input->post('id_pel_eda');
		if ($this->pelicula->eliminarPorId($id_pel_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Pelicula eliminada exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

		// redirect("asientoses/index");
	}




}//fin
