<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BoletosP extends CI_Controller {
  public function __construct(){
    parent:: __construct();
    $this->load->model("asiento");
    $this->load->model("bar");
    $this->load->model("tarifa");
    $this->load->model("usuario");
    $this->load->model("boleto");
    $this->load->model("pelicula");
  }//fin constructor

  public function ventaBoletos(){
    $data["listadoasientos"]=$this->asiento->obtenerTodos();
    $data["listadoPeliculas"]=$this->pelicula->obtenerTodos();
    $data["listadoTarifa"]=$this->tarifa->obtenerTodos();
    $data["asientos"]=$this->asiento->obtenerTodos();
		$this->load->view('publica/encabezado');
    $this->load->view("publica/boletos/listadoAsientos",$data);
		$this->load->view("publica/boletos/ventaBoletos",$data);
		$this->load->view("publica/pie");

	}

  public function guardarBoletos(){
  $datosBoleto=array(
    "fecha_emision_ven_eda"=>$this->input->post('fecha_emision_ven_eda'),
    "total_ven_eda"=>$this->input->post('total_ven_eda'),
    "fk_id_asi_eda"=>$this->input->post('fk_id_asi_eda'),
    "fk_id_pel_eda"=>$this->input->post('fk_id_pel_eda'),
    "fk_id_tar_eda"=>$this->input->post('fk_id_tar_eda')
  );
  // print_r($datosFunciones);
  if ($this->boleto->insertar($datosBoleto)) {
    $resultado=array(
    "estado"=>"ok", "mensaje"=>" insertado exitosamente"
    );
  }else {
    $resultado=array(
    "estado"=>"error", "mensaje"=>" nose pudo guardar los datos"
    );
  }
  echo json_encode($resultado);

}



}
