<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autenticaciones extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("autenticacion");


	}
  public function index(){
    $data["listadoslogs"]=$this->autenticacion->obtenerTodas();
    $this->load->view("administrador/encabezado");
    $this->load->view("administrador/autenticaciones/index",$data);
    $this->load->view("administrador/pie");
  }




}
