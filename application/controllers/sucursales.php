<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursales extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("sucursal");

	}

	public function editar($id){
		$data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
		$this->load->view('administrador/sucursales/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
			"provincia_suc_eda"=>$this->input->post('provincia_suc_eda'),
				"ciudad_suc_eda"=>$this->input->post('ciudad_suc_eda'),
				"estado_suc_eda"=>$this->input->post('estado_suc_eda'),
				"direccion_suc_eda"=>$this->input->post('direccion_suc_eda'),
				"email_suc_eda"=>$this->input->post('email_suc_eda'),
		);
		$id_suc_eda=$this->input->post('id_suc_eda');

		if ($this->sucursal->actualizar($id_suc_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Producto actualizado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}



  public function index(){

    $this->load->view("administrador/encabezado");
    $this->load->view("administrador/sucursales/index");
    $this->load->view("administrador/pie");
  }

// insercion/insertar de forma  asincrona
  public function guardar(){
    $datosSucursales=array(
      "provincia_suc_eda"=>$this->input->post('provincia_suc_eda'),
      "ciudad_suc_eda"=>$this->input->post('ciudad_suc_eda'),
      "estado_suc_eda"=>$this->input->post('estado_suc_eda'),
      "direccion_suc_eda"=>$this->input->post('direccion_suc_eda'),
      "email_suc_eda"=>$this->input->post('email_suc_eda')

    );
    if ($this->sucursal->insertar($datosSucursales)) {
      $resultado=array(
        'estado'=>'ok',
        'mensaje'=>'Sucursal insertado correctamente'
      );
    } else {
      $resultado=array(
        'estado'=>'error'
      );

    }
    echo json_encode($resultado);

  }


	//funcion para consultar profesores en formato json
	public function listado(){
		// $data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
		$data["sucursales"]=$this->sucursal->obtenerTodos();
		$this->load->view("administrador/sucursales/listado",$data);
	}

	public function borrar(){
		$id_suc_eda=$this->input->post('id_suc_eda');
		if ($this->sucursal->eliminarPorId($id_suc_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Sala eliminado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

	}

}//cierre
