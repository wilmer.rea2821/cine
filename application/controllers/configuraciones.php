<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("configuracion");
	}//fin constructor

	public function editar($id){
		$data["configuracionEditar"]=$this->configuracion->obtenerPorId($id);
		$this->load->view('administrador/configuraciones/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
			"nombre_empresa_con_eda"=>$this->input->post('nombre_empresa_con_eda'),
				"ruc_con_eda"=>$this->input->post('ruc_con_eda'),
				"telefono_con_eda"=>$this->input->post('telefono_con_eda'),
				"direccion_con_eda"=>$this->input->post('direccion_con_eda'),
				"representante_con_eda"=>$this->input->post('representante_con_eda'),
		);
    $id_con_eda=$this->input->post('id_con_eda');

		if ($this->configuracion->actualizar($id_con_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Confirguracion editada exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}


  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/configuraciones/index');
		$this->load->view('administrador/pie');
	}//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datosConfiguracion=array(
			"nombre_empresa_con_eda"=>$this->input->post('nombre_empresa_con_eda'),
	      "ruc_con_eda"=>$this->input->post('ruc_con_eda'),
	      "telefono_con_eda"=>$this->input->post('telefono_con_eda'),
	      "direccion_con_eda"=>$this->input->post('direccion_con_eda'),
	      "representante_con_eda"=>$this->input->post('representante_con_eda'),
    );

		$this->load->library("upload"); //Activando la libreria de subidas de archivos
		$new_name = "foto_" . time() . "_" . rand(1, 5000); //Generando un nombre aleatorio
		$config['file_name'] = $new_name; //
		$config['upload_path'] = FCPATH . 'uploads/config/';// configurando en cual carpeta se ba a subir el archivo
		$config['allowed_types'] = 'jpg|png|jpeg'; //dos extenciones especificas pdf|docx
		$config['max_size']  = 5*1024; //tamaño de la foto 2MB
		$this->upload->initialize($config);//inicializando la configuracion
		//Validando la subida de archivo
		if ($this->upload->do_upload("foto_con_eda")) {
			//Que se subio con exito
			$dataSubida = $this->upload->data(); //Capturar el dato que se a subido
			$datosConfiguracion["foto_con_eda"] = $dataSubida['file_name'];
		}

    if ($this->configuracion->insertar($datosConfiguracion)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Confirguración insertado exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		$data["configuraciones"]=$this->configuracion->obtenerTodos();
		$this->load->view("administrador/configuraciones/listado",$data);
	}//fin funcion listado

	public function borrar(){
		$id_con_eda=$this->input->post('id_con_eda');
		if ($this->configuracion->eliminarPorId($id_con_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Confirguración insertado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

		// redirect("configuraciones/index");
	}




}//fin
