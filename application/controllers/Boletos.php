<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Boletos extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("asiento");
    $this->load->model("bar");
    $this->load->model("tarifa");
    $this->load->model("usuario");
    $this->load->model("boleto");
		$this->load->model("pelicula");
	}
//
  public function index(){
    // $data["listadofunciones"]=$this->funcion->obtenerTodas();
    $data["listadoasientos"]=$this->asiento->obtenerTodos();
    $data["listadoPeliculas"]=$this->pelicula->obtenerTodos();
    $data["listadoTarifa"]=$this->tarifa->obtenerTodos();
    $this->load->view("administrador/encabezado");
    $this->load->view("administrador/boletos/index",$data);
    $this->load->view("administrador/pie");
  }



	// public function actualizar($id_fun){
	// 	$data["listadoPeliculas"]=$this->pelicula->obtenerTodos();
	// 	$data["listadoSalas"]=$this->sala->obtenerTodo();
	// 	$data["listadoHoras"]=$this->hora->obtenerTodo();
	// 	$data["funcionEditar"]=$this->funcion->obtenerPorId($id_fun);
	// 	$this->load->view("administrador/funciones/actualizar",$data);
  //
	// }




  public function guardarBoletos(){
  $datosBoleto=array(
    "fecha_emision_ven_eda"=>$this->input->post('fecha_emision_ven_eda'),
    "total_ven_eda"=>$this->input->post('total_ven_eda'),
    "fk_id_asi_eda"=>$this->input->post('fk_id_asi_eda'),
    "fk_id_pel_eda"=>$this->input->post('fk_id_pel_eda'),
    "fk_id_tar_eda"=>$this->input->post('fk_id_tar_eda')
  );
  // print_r($datosFunciones);
  if ($this->boleto->insertar($datosBoleto)) {
    $resultado=array(
    "estado"=>"ok", "mensaje"=>" insertado exitosamente"
    );
  }else {
    $resultado=array(
    "estado"=>"error", "mensaje"=>" nose pudo guardar los datos"
    );
  }
  echo json_encode($resultado);

}

public function borrar(){
	$id_con_eda=$this->input->post('id_ven_eda');

	if ($this->boleto->elimanarPorId($id_con_eda)) {
		$resultado=array(
			"estado"=>"ok",
			"mensaje"=>"Boletos eliminado exitosamente"
		);
		} else {
			$resultado=array(
				"estado"=>"error");
			}
			echo json_encode($resultado);
}

public function editar($id){
	// $data["listadofunciones"]=$this->funcion->obtenerTodas();
	$data["listadoasientos"]=$this->asiento->obtenerTodos();
	$data["listadoPeliculas"]=$this->pelicula->obtenerTodos();
	$data["listadoTarifa"]=$this->tarifa->obtenerTodos();
	$data["ventaBoletos"]=$this->boleto->obtenerPorId($id);
	$this->load->view("administrador/boletos/editar",$data);
}


public function procesoActualizar(){
	$datosBoletosEditado=array(
    "fecha_emision_ven_eda"=>$this->input->post('fecha_emision_ven_eda'),
    "total_ven_eda"=>$this->input->post('total_ven_eda'),
    "fk_id_asi_eda"=>$this->input->post('fk_id_asi_eda'),
    "fk_id_pel_eda"=>$this->input->post('fk_id_pel_eda'),
    "fk_id_tar_eda"=>$this->input->post('fk_id_tar_eda'),
    "fk_id_usu_eda"=>$this->input->post('fk_id_usu_eda')
	);
	$id_fun=$this->input->post("id_ven_eda");
	if ($this->boleto->actualizar($id_fun,$datosBoletosEditado)) {
		$resultado=array(
			"estado"=>"ok",
			"mensaje"=>"boletos editada exitosamente"
		);

	} else {
		$resultado=array(
			"estado"=>"error");
	}
	echo json_encode($resultado);

}

public function listado(){
	$data["listadoBoletos"]=$this->boleto->obtenerTodas();
	$this->load->view("administrador/boletos/listado",$data);
}



}
