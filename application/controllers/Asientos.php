<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asientos extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("asiento");
	}//fin constructor

	public function editar($id){
		$data["asientoEditar"]=$this->asiento->obtenerPorId($id);
		$this->load->view('administrador/asientos/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
			"fila_asi_eda"=>$this->input->post('fila_asi_eda'),
				"numero_asi_eda"=>$this->input->post('numero_asi_eda'),
		);
    $ind_asi_eda=$this->input->post('ind_asi_eda');

		if ($this->asiento->actualizar($ind_asi_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Asientos editada exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}


  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/asientos/index');
		$this->load->view('administrador/pie');
	}//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datosasientos=array(
			"fila_asi_eda"=>$this->input->post('fila_asi_eda'),
				"numero_asi_eda"=>$this->input->post('numero_asi_eda'),
    );

    if ($this->asiento->insertar($datosasientos)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Asiento insertado exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		$data["asientos"]=$this->asiento->obtenerTodos();
		$this->load->view("administrador/asientos/listado",$data);
	}//fin funcion listado

	public function borrar(){
		$ind_asi_eda=$this->input->post('id_asi_eda');
		if ($this->asiento->eliminarPorId($ind_asi_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Asiento insertado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

		// redirect("asientoses/index");
	}




}//fin
