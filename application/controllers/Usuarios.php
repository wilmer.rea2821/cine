
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("usuario");
	}//fin constructor

	public function editar($id){
		$data["usuariosEditar"]=$this->usuario->obtenerPorId($id);
		$this->load->view('administrador/usuarios/editar',$data);
	}
	public function actualizarUsuarios(){
		$datosEditar=array(
      "apellido_usu_eda"=>$this->input->post('apellido_usu_eda'),
      "nombre_usu_eda"=>$this->input->post('nombre_usu_eda'),
      "email_usu_eda"=>$this->input->post('email_usu_eda'),
      "password_usu_eda"=>$this->input->post('password_usu_eda'),
      "perfil_usu_eda"=>$this->input->post('perfil_usu_eda')
		);
    $id_usu_eda=$this->input->post('id_usu_eda');

		if ($this->usuario->actualizar($id_usu_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Usuario editado  exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}
  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/usuarios/index');
		$this->load->view('administrador/pie');
	}//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datosUsuario=array(
      "apellido_usu_eda"=>$this->input->post('apellido_usu_eda'),
      "nombre_usu_eda"=>$this->input->post('nombre_usu_eda'),
      "email_usu_eda"=>$this->input->post('email_usu_eda'),
      "password_usu_eda"=>$this->input->post('password_usu_eda'),
      "perfil_usu_eda"=>$this->input->post('perfil_usu_eda')
    );



    if ($this->usuario->insertar($datosUsuario)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Usuario insertado exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		$data["listadoUsuarios"]=$this->usuario->obtenerTodos();
		$this->load->view("administrador/usuarios/listado",$data);
	}//fin funcion listado

  public function borrar($id_usu_eda){
     $Usuarios=$this->usuario->obtenerPorId($id_usu_eda);
    if ($this->usuario->eliminarPorId($id_usu_eda)) {
      $this->session->set_flashdata('confirmacion','Eliminado Exitosamente');
      enviarEmail("alex.travez1364@utc.edu.ec",
     "NOTIFICACION_ELIMINACION",
     "<h1>El Usuario ".$Usuarios->apellido_usu_eda. " ".$Usuarios->nombre_usu_eda. "</h1> fue eliminado el ".date("Y-m-d H:i:s")
      );

    } else {
      $$this->session->set_flashdata('error eliminar','Verifique e intente nuevamente');

    }
     redirect("usuarios/index");
  }




}//fin
