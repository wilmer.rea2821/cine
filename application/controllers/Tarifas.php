<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarifas extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("tarifa");
	}//fin constructor

	public function editar($id){
		$data["tarifaEditar"]=$this->tarifa->obtenerPorId($id);
		$this->load->view('administrador/tarifas/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
			"adulto_tar_eda"=>$this->input->post('adulto_tar_eda'),
				"estudiante_tar_eda"=>$this->input->post('estudiante_tar_eda'),
				"ninos_tar_eda"=>$this->input->post('ninos_tar_eda'),
				"discapacitados_tar_eda"=>$this->input->post('discapacitados_tar_eda'),
				"tercera_tar_eda"=>$this->input->post('tercera_tar_eda'),
		);
    $id_tar_eda=$this->input->post('id_tar_eda');

		if ($this->tarifa->actualizar($id_tar_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Confirguracion editada exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}


  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/tarifas/index');
		$this->load->view('administrador/pie');
	}//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datosTarifa=array(
      "adulto_tar_eda"=>$this->input->post('adulto_tar_eda'),
				"estudiante_tar_eda"=>$this->input->post('estudiante_tar_eda'),
				"ninos_tar_eda"=>$this->input->post('ninos_tar_eda'),
				"discapacitados_tar_eda"=>$this->input->post('discapacitados_tar_eda'),
				"tercera_tar_eda"=>$this->input->post('tercera_tar_eda'),
    );
    if ($this->tarifa->insertar($datosTarifa)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"tarifa insertado exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		$data["tarifas"]=$this->tarifa->obtenerTodos();
		$this->load->view("administrador/tarifas/listado",$data);
	}//fin funcion listado

	public function borrar(){
		$id_tar_eda=$this->input->post('id_tar_eda');
		if ($this->tarifa->eliminarPorId($id_tar_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Confirguración insertado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

		// redirect("tarifas/index");
	}




}//fin
