<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salas extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("sala");
	}//fin constructor

	public function editar($id){
		$data["salaEditar"]=$this->sala->obtenerPorId($id);
		$this->load->view('administrador/salas/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
			"nombre_sal_eda"=>$this->input->post('nombre_sal_eda'),

		);
    $id_sal_eda=$this->input->post('id_sal_eda');

		if ($this->sala->actualizar($id_sal_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Sala actualizado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}


  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/salas/index');
		$this->load->view('administrador/pie');
	}//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datosSalas=array(
			"nombre_sal_eda"=>$this->input->post('nombre_sal_eda'),
    );
    if ($this->sala->insertar($datosSalas)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Sala ingresado exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		// $data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
		$data["listadoSala"]=$this->sala->obtenerTodos();
		$this->load->view("administrador/salas/listado",$data);
	}//fin funcion listado


	public function borrar(){
		$id_sal_eda=$this->input->post('id_sal_eda');
		if ($this->sala->eliminarPorId($id_sal_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Sala eliminado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

	}

}//cierre
