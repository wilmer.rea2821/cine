<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TarifasP extends CI_Controller {
  public function __construct(){
    parent:: __construct();
    $this->load->model("tarifa");
  }//fin constructor

  public function listadoTarifa(){
		// $data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
$data["tarifas"]=$this->tarifa->obtenerTodos();
		 $this->load->view('publica/encabezado');
		$this->load->view("publica/tarifas/listadoTarifa",$data);
		$this->load->view("publica/pie");

	}
}
