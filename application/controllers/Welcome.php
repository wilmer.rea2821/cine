<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
		$this->load->view('publica/encabezado');
		$this->load->view('welcome_message');
		 $this->load->view('publica/pie');
	}
	public function administrador()
	{
			 $this->load->view('administrador/encabezado');
			 $this->load->view('administrador/pie');
	}
}
