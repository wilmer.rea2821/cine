<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bares extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("bar");
	}//fin constructor

	public function editar($id){
		$data["barEditar"]=$this->bar->obtenerPorId($id);
		$this->load->view('administrador/bares/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
			"nombre_bar_eda"=>$this->input->post('nombre_bar_eda'),
				"precio_bar_eda"=>$this->input->post('precio_bar_eda'),
				"cantidad_bar_eda"=>$this->input->post('cantidad_bar_eda'),
		);
    $id_bar_eda=$this->input->post('id_bar_eda');

		if ($this->bar->actualizar($id_bar_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Producto actualizado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}


  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/bares/index');
		$this->load->view('administrador/pie');
	}//fin funcion index
//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datosbares=array(
			"nombre_bar_eda"=>$this->input->post('nombre_bar_eda'),
				"precio_bar_eda"=>$this->input->post('precio_bar_eda'),
				"cantidad_bar_eda"=>$this->input->post('cantidad_bar_eda'),
    );

    if ($this->bar->insertar($datosbares)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Producto ingresado exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		// $data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
		$data["listadobar"]=$this->bar->obtenerTodos();
		$this->load->view("administrador/bares/listado",$data);


	}//fin funcion listado



	public function borrar(){
		$id_bar_eda=$this->input->post('id_bar_eda');
		if ($this->bar->eliminarPorId($id_bar_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Producto eliminado exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

	}

}//cierre
