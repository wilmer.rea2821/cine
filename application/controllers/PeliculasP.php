<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeliculasP extends CI_Controller {
  public function __construct(){
    parent:: __construct();
    $this->load->model("pelicula");
  }//fin constructor

  public function listadoPelicula(){
		// $data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
$data["lisPeliculas"]=$this->pelicula->obtenerTodos();
		 $this->load->view('publica/encabezado');
		$this->load->view("publica/peliculas/listadoPelicula",$data);
		$this->load->view("publica/pie");

	}
}
