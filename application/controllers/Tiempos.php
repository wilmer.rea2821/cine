<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiempos extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("tiempo");
	}//fin constructor

	public function editar($id){
		$data["tiempoEditar"]=$this->tiempo->obtenerPorId($id);
		$this->load->view('administrador/tiempos/editar',$data);
	}
	public function actualizar(){
		$datosEditar=array(
			"hora_hor_eda"=>$this->input->post('hora_hor_eda'),
			"fecha_hor_eda"=>$this->input->post('fecha_hor_eda')
		);
    $id_hor_eda=$this->input->post('id_hor_eda');

		if ($this->tiempo->actualizar($id_hor_eda,$datosEditar)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Hora actualizada exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}//fin else
		echo json_encode($resultado);

	}


  //inicio funcion index
  public function index()
	{
		$this->load->view('administrador/encabezado');
		$this->load->view('administrador/tiempos/index');
		$this->load->view('administrador/pie');
	}//fin funcion index

  //inicio funcion guardar
  public function guardar()
  {
    $datosTiempos=array(
			"hora_hor_eda"=>$this->input->post('hora_hor_eda'),
			"fecha_hor_eda"=>$this->input->post('fecha_hor_eda')
    );
    if ($this->tiempo->insertar($datosTiempos)) {
      $resultado=array(
        "estado"=>"ok",
        "mensaje"=>"Hora ingresada exitosamente"
      );
    } else {
      $resultado=array(
        "estado"=>"error");
    }//fin else
    echo json_encode($resultado);
  }//fin funcion guardar y tiene insercion asincrona

	//funcion para listado profesores en formato JSON ($objeto)
	public function listado(){
		// $data["sucursalEditar"]=$this->sucursal->obtenerPorId($id);
		$data["listadoTiempos"]=$this->tiempo->obtenerTodo();
		$this->load->view("administrador/tiempos/listado",$data);
	}//fin funcion listado


	public function borrar(){
		$id_hor_eda=$this->input->post('id_hor_eda');
		if ($this->tiempo->eliminarPorId($id_hor_eda)) {
			$resultado=array(
				"estado"=>"ok",
				"mensaje"=>"Hora eliminada exitosamente"
			);
		} else {
			$resultado=array(
				"estado"=>"error");
		}
		echo json_encode($resultado);

	}

}//cierre
