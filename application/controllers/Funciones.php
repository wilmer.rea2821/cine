<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funciones extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("funcion");
    $this->load->model("pelicula");
    $this->load->model("sala");
    $this->load->model("tiempo");
	}
//
  public function index(){
    // $data["listadofunciones"]=$this->funcion->obtenerTodas();
    $data["listadoPeliculas"]=$this->pelicula->obtenerTodos();
    $data["listadoSalas"]=$this->sala->obtenerTodos();
    $data["listadoHoras"]=$this->tiempo->obtenerTodo();
    $this->load->view("administrador/encabezado");
    $this->load->view("administrador/funciones/index",$data);
    $this->load->view("administrador/pie");
  }



  public function guardarFunciones(){
  $datosFunciones=array(
    "nombre_funcion"=>$this->input->post('nombre_funcion'),
    "fk_id_pel_eda"=>$this->input->post('fk_id_pel_eda'),
    "fk_id_sal_eda"=>$this->input->post('fk_id_sal_eda'),
    "fk_id_hor_eda"=>$this->input->post('fk_id_hor_eda')
  );
  // print_r($datosFunciones);
  if ($this->funcion->insertar($datosFunciones)) {
    $resultado=array(
    "estado"=>"ok", "mensaje"=>" insertado exitosamente"
    );
  }else {
    $resultado=array(
    "estado"=>"error", "mensaje"=>" nose pudo guardar los datos"
    );
  }
  echo json_encode($resultado);

}

public function borrar(){
	$id=$this->input->post('id_fun_eda');
	if ($this->funcion->elimanarPorId($id)) {
		$resultado=array(
			"estado"=>"ok",
			"mensaje"=>"Confirguración insertado exitosamente"
		);
		} else {
			$resultado=array(
				"estado"=>"error");
				}
	echo json_encode($resultado);
}

public function editar($id){
	$data["listadoPeliculas"]=$this->pelicula->obtenerTodos();
	$data["listadoSalas"]=$this->sala->obtenerTodos();
	$data["listadoHoras"]=$this->tiempo->obtenerTodo();
	$data["funcionEditar"]=$this->funcion->obtenerPorId($id);
	$this->load->view("administrador/funciones/editar",$data);
}


public function procesoActualizar(){
	$datosFuncionesEditado=array(
		"nombre_funcion"=>$this->input->post('nombre_funcion'),
		"fk_id_pel_eda"=>$this->input->post('fk_id_pel_eda'),
		"fk_id_sal_eda"=>$this->input->post('fk_id_sal_eda'),
		"fk_id_hor_eda"=>$this->input->post('fk_id_hor_eda'),
	);
	 $id_fun_eda=$this->input->post("id_fun_eda");
	if ($this->funcion->actualizar($id_fun_eda,$datosFuncionesEditado)) {
		$resultado=array(
			"estado"=>"ok",
			"mensaje"=>"Confirguracion editada exitosamente"
		);
		// redirect('funciones/index');

	} else {
		$resultado=array(
			"estado"=>"error");
	}
	echo json_encode($resultado);
}

public function listado(){
	$data["listadofunciones"]=$this->funcion->obtenerTodas();
	$this->load->view("administrador/funciones/listado",$data);
}


}
